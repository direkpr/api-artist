<?php

namespace App\Action\Payment;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use App\Domain\Omise\Service\Customer;

final class InvoiceReadAction
{
    private $customer;
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        $invoice = $args['invoice'];
        if($data = $this->customer->invoice($invoice)){
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>true,
                    "data"=>$data
                )
            ));
        }else{
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>false,
                    "message"=>"Invoice number not found!"
                )
            ));
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}