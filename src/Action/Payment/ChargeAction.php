<?php

namespace App\Action\Payment;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use App\Domain\Omise\Service\Customer;

final class ChargeAction
{
    private $customer;
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $data = (array)$request->getParsedBody();
        if($this->customer->charge($data)){
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>true,
                    "message"=>"Omise charge success",
                    "invoice"=>$this->customer->invoiceById($data['invoiceId'])
                )
            ));
        }else{
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>false,
                    "message"=>"Omise charge fail"
                )
            ));
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}