<?php

namespace App\Action\Payment;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use OmiseAccount;
use OmiseCustomer;
use OmiseCharge;
use OmiseSchedule;
use DateTime;
use DateInterval;

final class TestAction
{
    public function __construct()
    {
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $date = new DateTime('2020-07-10');
        $preDate = new DateTime('2020-07-10'.' 23:59:59');
        $preDate->sub(new DateInterval('P1D'));

        var_dump($date);
        var_dump($preDate->format("Y-m-d"));
        $schedule    = OmiseSchedule::retrieve('schd_test_5m2c0btkatsjd3006wv');
        $occurrences = $schedule->occurrences(array("order"=>"reverse_chronological"));
        var_dump($occurrences['data'][0]);

        /*$account = OmiseAccount::retrieve();
        //$charge = OmiseCharge::retrieve('chrg_test_5lmedrj24nscchb44ya');
        //var_dump($charge->offsetGet('card')['last_digits']);

        $omise_customer = OmiseCustomer::retrieve("cust_test_5lzpakhrgk0zlu5lbho");
        var_dump($omise_customer);
        $default_card = $omise_customer->offsetGet('default_card');
        $card = $omise_customer->getCards()->retrieve($default_card);
        var_dump($card);
        $brand = $card->offsetGet('brand');
        $last_digits = $card->offsetGet('last_digits');
        var_dump([$brand,$last_digits]);*/

        $response->getBody()->write((string)json_encode(
                array(
                    "status"=>true,
                    "message"=>"Omise test success",
                    //"email"=>$account['email']
                )
            )
        );
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}