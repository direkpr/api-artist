<?php

namespace App\Action;
use Slim\App;
use App\Domain\Member\Service\MemberCredentialReader;
use App\Domain\Member\Service\MemberReader;
use App\Domain\Omise\Service\Customer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use \Firebase\JWT\JWT;
use DomainException;
final class MemberLoginAction
{
    private $credentialReader;
    private $memberReader;
    private $app;
    private $customer;
    public function __construct(MemberCredentialReader $credentialReader, MemberReader $memberReader,App $app,Customer $customer)
    {
        $this->credentialReader = $credentialReader;
        $this->memberReader = $memberReader;
        $this->app = $app;
        $this->customer = $customer;
    }
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();
        if(empty($data['email']) || empty($data['password'])){
            $response->getBody()->write((string)json_encode(array("status"=>false,"message"=>"email or password has require!")));
            return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        }
        try {
            $member = $this->memberReader->getMemberByEmail($data['email']);
            if($member->id && $member->payment_status == 1){
                
                //getCustomer
                $customer = $this->memberReader->getCustomer($member->id);
                if($customer && $customer['subscription_type'] == 2){
                    //check member subscriber
                    $omise_schedule_id = $customer['omise_schedule_id'];
                    if(!is_null($omise_schedule_id)){
                        $this->customer->updateSchedulCustomer($omise_schedule_id);
                    }else{
                        $this->memberReader->setSubscripbeToOneTime($member->id);
                    }
                }
                $this->memberReader->updateCustomerStatus($member->id);

                $credential = $this->credentialReader->getCredentialByMemberId($member->id);
                if($credential){
                    if(md5($data['password']) == $credential->password){
                        $now_seconds = time();
                        $payload = array(
                            "iss" => $member->email,
                            "sub" => $member->email,
                            "iat" => $now_seconds,
                            "exp" => $now_seconds+(60*60),  // Maximum expiration time is one hour
                            "uid" => $member->id,
                            "data" => $member
                        );
                        $response->getBody()->write((string)json_encode(
                            array(
                                "status"=>true,
                                "message"=>"login successful",
                                "member"=>$member,
                                "roles"=>$this->memberReader->getRoles($member->id),
                                "token"=>JWT::encode($payload,$this->app->getContainer()->get('settings')['secret']))));
                        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
                    }else{
                        $response->getBody()->write((string)json_encode(array("status"=>false,"message"=>"password not correct!")));
                        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
                    }
                }
            }else{
                $response->getBody()->write((string)json_encode(array("status"=>false,"message"=>"email not correct!")));
                return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
            }
        } catch (DomainException $e ){
            $response->getBody()->write((string)json_encode(array("status"=>false,"message"=>"email or password not correct!")));
            return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        }
        


        // Build the HTTP response
        $response->getBody()->write((string)json_encode(array("status"=>false,"message"=>"unknow error !")));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}