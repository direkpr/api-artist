<?php

namespace App\Action;

use App\Domain\Member\Service\MemberReader;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action
 */
final class MemberAdminAllAction
{
    /**
     * @var MemberReader
     */
    private $memberReader;

    /**
     * The constructor.
     *
     * @param UserReader $memberReader The member reader
     */
    public function __construct(MemberReader $memberReader)
    {
        $this->memberReader = $memberReader;
    }

    /**
     * Invoke.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     * @param array $args The route arguments
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {

        $params = $request->getQueryParams();
        //var_dump($params);
        try {
            $page = floor(intval($params['offset']) / intval($params['limit']));
            $totalCount = $this->memberReader->get_admin_total_count($params);
            $data = $this->memberReader->getMemberAdminAlls($params);
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(array("data"=>$data,"page"=>$page,"totalCount"=>$totalCount)));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);

        } catch (ValidationException $e){
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(
                array("status"=>false,"message"=>"List Album fail","errors"=>$e->getErrors())
            ));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }

        /*
        $userData = $this->memberReader->getMemberAdminAlls();
        // Build the HTTP response
        $response->getBody()->write((string)json_encode($userData));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
        */
    }
}