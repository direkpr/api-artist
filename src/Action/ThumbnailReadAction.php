<?php
namespace App\Action;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use \Firebase\JWT\JWT;
use Intervention\Image\ImageManagerStatic as Image;
use App\Factory\LoggerFactory;

final class ThumbnailReadAction
{
    private $container;
    private $logger;
    public function __construct(App $app,LoggerFactory $logger)
    {
        $this->container = $app->getContainer();
        $this->logger = $logger
            ->addFileHandler('ThumbnailReadAction.log')
            ->createInstance('ThumbnailReadAction');
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $directory = $this->container->get('settings')['upload_directory'];
        $directory_tmp = $this->container->get('settings')['temp'];
        $key = $this->container->get('settings')['secret'];
        $params = $request->getQueryParams();
        $filename = (string)$params['f'];
        $token = (string)$params['key'];
        //$this->logger->info(print_r($params,true));
        if($token){
            $decoded = JWT::decode($token, $key, array('HS256'));
            $this->logger->info(print_r($decoded,true));
            $roles = $decoded->data->roles;
            $this->logger->info(print_r($roles,true));
            if(in_array("customer",$roles)){
                //$image = Image::make($directory . DIRECTORY_SEPARATOR . $filename)->fit(444,null)->resizeCanvas(444, 614);
                $image = Image::make($directory . DIRECTORY_SEPARATOR . $filename);
            }else{
                //$image = Image::make($directory . DIRECTORY_SEPARATOR . $filename)->fit(444,null)->resizeCanvas(444, 614)->insert($directory_tmp . DIRECTORY_SEPARATOR . 'copyright.png');
                $image = Image::make($directory . DIRECTORY_SEPARATOR . $filename)->insert($directory_tmp . DIRECTORY_SEPARATOR . 'copyright.png');
            }
        }else{
            //$image = Image::make($directory . DIRECTORY_SEPARATOR . $filename)->fit(444,null)->resizeCanvas(444, 614)->insert($directory_tmp . DIRECTORY_SEPARATOR . 'copyright.png');
            $image = Image::make($directory . DIRECTORY_SEPARATOR . $filename)->insert($directory_tmp . DIRECTORY_SEPARATOR . 'copyright.png');
        }
        if(!file_exists($directory . DIRECTORY_SEPARATOR . $filename))
            die();

        echo $image->encode();
        //$image = file_get_contents($directory . DIRECTORY_SEPARATOR . $filename);
        
        // Build the HTTP response
        //$response->getBody()->write();
        return $response
            ->withHeader('Content-Type', mime_content_type($directory . DIRECTORY_SEPARATOR . $filename))
            ->withStatus(201);
    }
}