<?php

namespace App\Action;

use App\Domain\Langs\Service\LangsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class LangsReadAction
{

    private $service;

    public function __construct(LangsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $params = $request->getQueryParams();
        $isActive = false;
        if(isset($params['isActive'])){
            $isActive = boolval($params['isActive']);
        }
        // Invoke the Domain with inputs and retain the result
        $langs = $this->service->getAllLangs($isActive);
        // Transform the result into the JSON representation
        // Build the HTTP response
        $response->getBody()->write((string)json_encode($langs));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}