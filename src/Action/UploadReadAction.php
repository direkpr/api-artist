<?php
namespace App\Action;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use \Firebase\JWT\JWT;
use Intervention\Image\ImageManagerStatic as Image;

final class UploadReadAction
{
    private $container;
    public function __construct(App $app)
    {
        $this->container = $app->getContainer();
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $directory = $this->container->get('settings')['upload_directory'];
        $directory_tmp = $this->container->get('settings')['temp'];
        $params = $request->getQueryParams();
        $filename = (string)$params['f'];
        $token = (string)$params['key'];
        if($token){

        }
        if(!file_exists($directory . DIRECTORY_SEPARATOR . $filename))
            die();

        
        //$image = file_get_contents($directory . DIRECTORY_SEPARATOR . $filename);
        $image = Image::make($directory . DIRECTORY_SEPARATOR . $filename);
        echo $image->encode();
        if ($image === false)
            die();
        // Build the HTTP response
        //$response->getBody()->write($image->encode());
        return $response
            ->withHeader('Content-Type', mime_content_type($directory . DIRECTORY_SEPARATOR . $filename))
            ->withStatus(201);
    }
}