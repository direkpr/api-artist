<?php

namespace App\Action;

use App\Domain\News\Service\NewsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class NewsCreateAction
{
    private $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();
        try {
            // Invoke the Domain with inputs and retain the result
            $Id = $this->service->createNews($data);
            // Transform the result into the JSON representation
            $result = [
                'id' => $Id
            ];
            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);

        } catch (ValidationException $e){
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(
                array("status"=>false,"message"=>"Create News fail","errors"=>$e->getErrors())
            ));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
}