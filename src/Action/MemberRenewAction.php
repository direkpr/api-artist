<?php

namespace App\Action;

use App\Domain\Member\Service\MemberCreator;
use App\Domain\Omise\Service\Customer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use DomainException;

final class MemberRenewAction
{
    private $memberCreator;
    private $customer;

    public function __construct(MemberCreator $memberCreator,Customer $customer)
    {
        $this->memberCreator = $memberCreator;
        $this->customer = $customer;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();
        
        try {
            // Invoke the Domain with inputs and retain the result
            $result = $this->memberCreator->renewMember($data);

            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
        catch(ValidationException $e){
            // Transform the result into the JSON representation
            $result = [
                "status"=>false,
                "message"=>"Renew subscription faile",
                "errors" => $e->getErrors()
            ];
            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
}