<?php

namespace App\Action;

use App\Domain\News\Service\NewsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class ShareNewsAction
{
    private $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        $id = (int)$args['id'];
        $data = $this->service->getById($id);
        $title = $data->texts[0]['title'];
        $description = $data->shareContent;
        $image = $data->imgShare;
        //<meta name="twitter:image" content="http://euro-travel-example.com/thumbnail.jpg">
        //<meta property="og:image" content="https://i.insider.com/5deac5d0fd9db248193429ff?width=900&format=jpeg&auto=webp">
$html = <<<EOD
<!DOCTYPE html>
<html>
<head>
<title>REALME - AYANA MIYAKE</title>
<meta name="description" content="{$description}">
<meta property="og:title" content="{$title}">
<meta property="og:description" content="{$description}">
<meta property="og:image" content="http://www.realmedemo.eqho.com/php{$image}">
<meta name="twitter:title" content="{$title}">
<meta name="twitter:description" content="{$description}">
<meta name="twitter:image" content="http://www.realmedemo.eqho.com/php{$image}">
<meta name="twitter:card" content="summary_large_image">
</head>
<body>
<iframe src="http://realmedemo.eqho.com/news/{$id}" style="position:fixed; top:0; left:0; bottom:0; right:0; width:100%; height:100%; border:none; margin:0; padding:0; overflow:hidden; z-index:999999;">
    Your browser doesn't support iframes
</iframe>
</body>
</html>
EOD;
        $response->getBody()->write($html);
        return $response
            ->withHeader('Content-Type', 'text/html')
            ->withStatus(200);
    }
}