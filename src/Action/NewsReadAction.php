<?php

namespace App\Action;

use App\Domain\News\Service\NewsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class NewsReadAction
{
    private $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        $id = (int)$args['id'];
        $data = $this->service->getById($id);
        $response->getBody()->write((string)json_encode($data));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}