<?php

namespace App\Action;

use App\Domain\Goods\Service\GoodsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class GoodsReadAction
{
    private $service;

    public function __construct(GoodsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        $id = (int)$args['id'];
        $data = $this->service->getById($id);
        $response->getBody()->write((string)json_encode($data));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}