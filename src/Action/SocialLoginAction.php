<?php

namespace App\Action;
use Slim\App;
use App\Domain\Member\Service\MemberCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use App\Domain\Member\Service\MemberReader;
use App\Domain\Omise\Service\Customer;
use \Firebase\JWT\JWT;

final class SocialLoginAction
{
    private $service;
    private $memberReader;
    private $customer;
    private $app;
    public function __construct(MemberCreator $service,MemberReader $memberReader,Customer $customer,App $app)
    {
        $this->service = $service;
        $this->memberReader = $memberReader;
        $this->customer = $customer;
        $this->app = $app;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();
        $socialdata = $this->service->SocialLogin($data);
        $result = array("status"=>false,"message"=>"Login fail");
        
        if($socialdata && isset($socialdata['member_id'])){
            $member = $this->memberReader->getMemberById($socialdata['member_id']);
            if($member->id && $member->payment_status == 1){
                //getCustomer
                $customer = $this->memberReader->getCustomer($member->id);
                if($customer && $customer['subscription_type'] == 2){
                    //check member subscriber
                    $omise_schedule_id = $customer['omise_schedule_id'];
                    if(!is_null($omise_schedule_id)){
                        $this->customer->updateSchedulCustomer($omise_schedule_id);
                    }else{
                        $this->memberReader->setSubscripbeToOneTime($member->id);
                    }
                }
                $this->memberReader->updateCustomerStatus($member->id);
                $now_seconds = time();
                        $payload = array(
                            "iss" => $member->email,
                            "sub" => $member->email,
                            "iat" => $now_seconds,
                            "exp" => $now_seconds+(60*60),  // Maximum expiration time is one hour
                            "uid" => $member->id,
                            "data" => $member
                        );
                        $response->getBody()->write((string)json_encode(
                            array(
                                "status"=>true,
                                "message"=>"login successful",
                                "member"=>$member,
                                "roles"=>$this->memberReader->getRoles($member->id),
                                "token"=>JWT::encode($payload,$this->app->getContainer()->get('settings')['secret']))));
                        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
            }else{
                $response->getBody()->write((string)json_encode(array("status"=>false,"message"=>"email not correct!")));
                return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
            }
        
        }
        $response->getBody()->write((string)json_encode($result));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}