<?php

namespace App\Action\Site;

use App\Domain\News\Service\NewsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class NewsListAction
{
    private $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $params = $request->getQueryParams();
        //var_dump($params);
        try {
            $page = floor(intval($params['offset']) / intval($params['limit']));
            $totalCount = $this->service->get_active_count($params);
            $data = $this->service->get_active_news($params);
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(array("data"=>$data,"page"=>$page,"totalCount"=>$totalCount)));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);

        } catch (ValidationException $e){
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(
                array("status"=>false,"message"=>"List News fail","errors"=>$e->getErrors())
            ));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
}