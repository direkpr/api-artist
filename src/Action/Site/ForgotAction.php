<?php

namespace App\Action\Site;

use App\Domain\Member\Service\MemberReader;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class ForgotAction
{

    private $memberReader;
    public function __construct(MemberReader $memberReader)
    {
        $this->memberReader = $memberReader;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $data = (array)$request->getParsedBody();
        $res = $this->memberReader->forgotpassword($data);

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($res));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}