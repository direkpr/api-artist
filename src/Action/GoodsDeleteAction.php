<?php

namespace App\Action;

use App\Domain\Goods\Service\GoodsService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class GoodsDeleteAction
{
    private $service;

    public function __construct(GoodsService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        // Collect input from the HTTP request
        $id = (int)$args['id'];
        try {
            // Invoke the Domain with inputs and retain the result
            $this->service->deleteGoods($id);
            // Transform the result into the JSON representation
            $result = [
                'status' => true
            ];
            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);

        } catch (ValidationException $e){
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(
                array("status"=>false,"message"=>"Create goods fail","errors"=>$e->getErrors())
            ));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
}