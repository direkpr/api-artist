<?php

namespace App\Action;

use App\Domain\Member\Service\MemberCreator;
use App\Domain\Omise\Service\Customer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use DomainException;

final class MemberUpdateAction
{
    private $memberCreator;
    private $customer;

    public function __construct(MemberCreator $memberCreator,Customer $customer)
    {
        $this->memberCreator = $memberCreator;
        $this->customer = $customer;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response,
        array $args = []
    ): ResponseInterface {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();
        $userId = (int)$args['id'];
        try {
            // Invoke the Domain with inputs and retain the result
            $status = $this->memberCreator->updateMember($userId,$data);
            if( $status){
                $result = [
                    "status"=>true,
                    "message"=>"Update profile successful"
                ];
            }else{
                $result = [
                    "status"=>false,
                    "message"=>"Update data fail"
                ];
            }
            

            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
        catch(ValidationException $e){
            // Transform the result into the JSON representation
            $result = [
                "status"=>false,
                "message"=>"save data faile",
                "errors" => $e->getErrors()
            ];

            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }

        
    }
}