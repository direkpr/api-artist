<?php

namespace App\Action;

use App\Domain\Video\Service\VideoService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;

final class VideoListAction
{
    private $service;

    public function __construct(VideoService $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        $params = $request->getQueryParams();
        //var_dump($params);
        try {
            $page = floor(intval($params['offset']) / intval($params['limit']));
            $totalCount = $this->service->get_total_count($params);
            $data = $this->service->get_data($params);
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(array("data"=>$data,"page"=>$page,"totalCount"=>$totalCount)));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);

        } catch (ValidationException $e){
            // Build the HTTP response
            $response->getBody()->write((string)json_encode(
                array("status"=>false,"message"=>"List Video fail","errors"=>$e->getErrors())
            ));
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
    }
}