<?php

namespace App\Action;

use App\Domain\Member\Service\MemberReader;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action
 */
final class EBirthdaySendAllAction
{
    /**
     * @var MemberReader
     */
    private $memberReader;

    /**
     * The constructor.
     *
     * @param UserReader $memberReader The member reader
     */
    public function __construct(MemberReader $memberReader)
    {
        $this->memberReader = $memberReader;
    }

    /**
     * Invoke.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     * @param array $args The route arguments
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $userData = $this->memberReader->getEBirthdaySendAll();
        // Build the HTTP response
        $response->getBody()->write((string)json_encode($userData));
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}