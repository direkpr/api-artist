<?php

namespace App\Action;

use App\Domain\Member\Service\MemberCreator;
use App\Domain\Omise\Service\Customer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use DomainException;

final class MemberCreateAction
{
    private $memberCreator;
    private $customer;

    public function __construct(MemberCreator $memberCreator,Customer $customer)
    {
        $this->memberCreator = $memberCreator;
        $this->customer = $customer;
    }

    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        // Collect input from the HTTP request
        $data = (array)$request->getParsedBody();
        try {
            // Invoke the Domain with inputs and retain the result
            $userId = $this->memberCreator->createMember($data);
            $customer = $this->customer->createCustomer(array(
                "member_id"=>$userId,
                "firstname"=>$data['firstname'],
                "lastname"=>$data['lastname'],
                "email"=>$data['email'],
                "subscription_type"=>$data['subscription_type']
            ));

            //generate invoice for purchase
            $invoice = $this->customer->createInvoice($userId,($data['subscription_type'] == 2) ? MONTHLY_PURCHASE_AMT : ONETIME_PURCHASE_AMT,CURRENCY_PURCHASE,0,$data['subscription_type']);
            
            // Transform the result into the JSON representation
            $result = [
                "status"=>true,
                "message"=>"save data successful",
                'memberId' => $userId,
                'customer'=>$customer,
                'invoice'=>$invoice
            ];

            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }
        catch(ValidationException $e){
            // Transform the result into the JSON representation
            $result = [
                "status"=>false,
                "message"=>"save data faile",
                "errors" => $e->getErrors()
            ];

            // Build the HTTP response
            $response->getBody()->write((string)json_encode($result));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(200);
        }

        
    }
}