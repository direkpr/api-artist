<?php

namespace App\Action\Jobs;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use App\Domain\Member\Service\MemberReader;

final class SendEBirthdayCardAction
{
    private $service;
    public function __construct(MemberReader $service)
    {
        $this->service = $service;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        if($this->service->send_ebirthday_cards()){
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>true,
                    "message"=>"update success"
                )
            ));
        }else{
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>false,
                    "message"=>"update fail"
                )
            ));
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}