<?php

namespace App\Action\Jobs;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Exception\ValidationException;
use App\Domain\Omise\Service\Customer;

final class UpdateRecurringAction
{
    private $customer;
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }
    public function __invoke(
        ServerRequestInterface $request, 
        ResponseInterface $response
    ): ResponseInterface {
        if($this->customer->updateRecurring()){
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>true,
                    "message"=>"update success"
                )
            ));
        }else{
            $response->getBody()->write((string)json_encode(
                array(
                    "status"=>false,
                    "message"=>"update fail"
                )
            ));
        }
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}