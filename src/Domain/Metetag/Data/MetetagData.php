<?php
namespace App\Domain\Metetag\Data;
final class MetetagData
{
    public $id;
    public $title;
    public $keywords;
    public $description;
}