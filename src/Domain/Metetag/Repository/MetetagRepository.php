<?php
namespace App\Domain\Metetag\Repository;
use PDO;

class MetetagRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'title' => ($data['title']) ? $data['title'] : "",
            'keywords' => ($data['keywords']) ? $data['keywords'] : "",
            'description' => ($data['description']) ? $data['description'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE metetags SET 
                title=:title, 
                keywords=:keywords, 
                description=:description, 
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function getById(int $id){
        $sql = "select id,title,keywords,description from metetags where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Metetags not found: %s', $id));
        }
        return $row;
    }
}
