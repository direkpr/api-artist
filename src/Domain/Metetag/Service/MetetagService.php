<?php

namespace App\Domain\Metetag\Service;
use App\Factory\LoggerFactory;
use App\Domain\Metetag\Data\MetetagData;
use App\Domain\Metetag\Repository\MetetagRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class MetetagService
{

    private $repository;
    private $logger;

    public function __construct(MetetagRepository $repository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger
            ->addFileHandler('MetetagService.log')
            ->createInstance('MetetagService');
    }
    public function getById(int $id) : MetetagData {
        $data = $this->repository->getById($id);
        $return = new MetetagData();
        $return->id = $data['id'];
        $return->title = $data['title'];
        $return->keywords = $data['keywords'];
        $return->description = $data['description'];
        return $return;
    }

    public function updateMetetag(int $id,array $data){
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "title"=>$olddata->title,
            "keywords"=>$olddata->keywords,
            "description"=>$olddata->description
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        $this->repository->update($id,$oldarray);
    }
    
}