<?php
namespace App\Domain\Video\Repository;
use PDO;

class VideoTextRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'video_id' => $data['video_id'],
            'name' => ($data['name']) ? $data['name'] : "",
            'price' => ($data['price']) ? $data['price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "INSERT INTO videos_txt SET 
                video_id=:video_id, 
                name=:name, 
                price=:price, 
                lg=:lg";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'video_id' => $data['video_id'],
            'name' => ($data['name']) ? $data['name'] : "",
            'price' => ($data['price']) ? $data['price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "UPDATE videos_txt SET 
                video_id=:video_id, 
                name=:name, 
                price=:price, 
                lg=:lg WHERE id =:id";
        $this->connection->prepare($sql)->execute($row);
    }
    public function getByVideoId(int $id) {
        $sql = "select id,video_id,name,price,lg from videos_txt where video_id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        if (!$rows) {
            $rows = [];
        }
        return $rows;
    }
    public function deleteByVideoId(int $id){
        $sql = "DELETE FROM `videos_txt` WHERE `video_id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
