<?php
namespace App\Domain\Video\Repository;
use PDO;

class VideoRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'coverUrl' => ($data['coverUrl']) ? $data['coverUrl'] : "",
            'thumUrl' => ($data['thumUrl']) ? $data['thumUrl'] : "",
            'stream_preview' => ($data['stream_preview']) ? $data['stream_preview'] : "",
            'stream_full' => ($data['stream_full']) ? $data['stream_full'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'year' => ($data['year']) ? $data['year'] : 0,
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO videos SET 
                coverUrl=:coverUrl, 
                thumUrl=:thumUrl, 
                stream_preview=:stream_preview, 
                stream_full=:stream_full, 
                link=:link, 
                year=:year, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function updateOrder($id,$order_item){
        $row = [
            'id' => $id,
            'order_item' => ($order_item) ? $order_item : 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE videos SET 
                order_item=:order_item, 
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'coverUrl' => ($data['coverUrl']) ? $data['coverUrl'] : "",
            'thumUrl' => ($data['thumUrl']) ? $data['thumUrl'] : "",
            'stream_preview' => ($data['stream_preview']) ? $data['stream_preview'] : "",
            'stream_full' => ($data['stream_full']) ? $data['stream_full'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'year' => ($data['year']) ? $data['year'] : 0,
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE videos SET 
                coverUrl=:coverUrl, 
                thumUrl=:thumUrl, 
                stream_preview=:stream_preview, 
                stream_full=:stream_full, 
                link=:link, 
                year=:year, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function get_total_count($search) : int 
    {
        $sql = "select count(*) as num from videos where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $search = $param['search'];
            $where = "where a.datasearch like '%{$search}%'";
        }

        if(isset($param['where'])){
            $json_where = json_decode($param['where']);
            
            $where_arr = [];
            foreach($json_where as $key => $value){
                array_push($where_arr,"a.".$key."=".$value);
            }
            $where_str = join(" and ",$where_arr);
            //error_log( $where_str );
            if(strlen($where) > 0){
                $where = $where." and ".$where_str;
            }else{
                $where = "where ".$where_str;
            }
        }

        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,a.coverUrl,a.thumUrl,b.name,b.price,a.stream_preview,a.stream_full,a.link,a.year,a.createdtime,a.updatedtime,a.isActive FROM `videos` a left JOIN videos_txt b on a.id = b.video_id and b.lg = '{$lg}' {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,coverUrl,thumUrl,stream_preview,stream_full,link,year,isActive from videos where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Video not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `videos` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /** method for front-end */
    public function get_active_video(){
        $sql = "select id,coverUrl,thumUrl,stream_preview,stream_full,link,year,isActive,order_item from videos where isActive = 1 order by year desc,order_item asc";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
}
