<?php

namespace App\Domain\Video\Service;
use App\Factory\LoggerFactory;
use App\Domain\Video\Data\VideoData;
use App\Domain\Video\Repository\VideoRepository;
use App\Domain\Video\Repository\VideoTextRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class VideoService
{

    private $repository;
    private $textRepository;
    private $langsRepository;
    private $logger;

    public function __construct(VideoRepository $repository,VideoTextRepository $textRepository,LangsRepository $langsRepository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->textRepository = $textRepository;
        $this->langsRepository = $langsRepository;

        $this->logger = $logger
            ->addFileHandler('VideoService.log')
            ->createInstance('VideoService');
    }
    public function deleteVideo(int $id){
        $this->repository->delete($id);
        $this->textRepository->deleteByVideoId($id);
    }
    public function getById(int $id) : VideoData {
        $data = $this->repository->getById($id);
        $return = new VideoData();
        $return->id = $data['id'];
        $return->coverUrl = $data['coverUrl'];
        $return->thumUrl = $data['thumUrl'];
        $return->stream_preview = $data['stream_preview'];
        $return->stream_full = $data['stream_full'];
        $return->link = $data['link'];
        $return->year = $data['year'];
        $return->isActive = $data['isActive'];
        $return->texts = $this->textRepository->getByVideoId((int)$data['id']);
        return $return;
    }
    public function createVideo(array $data): int
    {
        // Input validation
        $this->validateNewVideo($data);
        $datasearch = '';
        foreach($data['texts'] as $text){
            $datasearch .= $text['name'].$text['price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            foreach($data['texts'] as $text){
                $text['video_id'] = $insert_id;
                if($this->validateTextVideo($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
        return $insert_id;
    }
    public function updateVideo(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "coverUrl"=>$olddata->coverUrl,
            "thumUrl"=>$olddata->thumUrl,
            "stream_preview"=>$olddata->stream_preview,
            "stream_full"=>$olddata->stream_full,
            "link"=>$olddata->link,
            "year"=>$olddata->year,
            "isActive"=>$olddata->isActive,
            "texts"=>$olddata->texts
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        //$this->logger->info(print_r($oldarray,true));
        $this->validateNewVideo($oldarray);
        $datasearch = '';
        foreach($oldarray['texts'] as $text){
            $datasearch .= $text['name'].$text['price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $oldarray['datasearch'] = $datasearch;
        $this->repository->update($id,$oldarray);
        foreach($oldarray['texts'] as $text){
            if(isset($text['id'])){
                //update
                $text['video_id'] = $id;
                if($this->validateTextVideo($text)){
                    $this->textRepository->update($text['id'],$text);
                }
            }else{
                $text['video_id'] = $id;
                if($this->validateTextVideo($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $lang = $this->langsRepository->get_default_lang();
        $params['lg'] = $lang->code;
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    private function validateTextVideo(array $data): bool {
        if(empty($data['lg'])){
            return false;
        }
        if(empty($data['name'])){
            return false;
        }
        if(empty($data['price'])){
            return false;
        }
        return true;
    }

    private function validateNewVideo(array $data): void
    {
        $errors = [];

        if (empty($data['coverUrl'])) {
            $errors['coverUrl'] = 'Input required';
        }
        if (empty($data['thumUrl'])) {
            $errors['thumUrl'] = 'Input required';
        }
        if (empty($data['stream_preview'])) {
            $errors['stream_preview'] = 'Input required';
        }

        if (empty($data['stream_full'])) {
            $errors['stream_full'] = 'Input required';
        }

        if (empty($data['link'])) {
            $errors['link'] = 'Input required';
        }

        if (empty($data['year'])) {
            $errors['year'] = 'Input required';
        }

        if(empty($data['texts'])){
            $errors['texts'] = 'Input required';
        }

        $lang = $this->langsRepository->get_default_lang();
        if(!$lang){
            $errors['lang'] = "default lang not found";
            throw new ValidationException('default lang not found', $errors);
        }
        //var_dump($lang['code']);
        $default_text = null;
        foreach($data['texts'] as $text){
            if($lang['code'] == $text['lg']){
                $default_text = $text;
            }
        }
        //var_dump($default_text);
        if(is_null($default_text)){
            $errors['texts'] = 'default text not found';
            throw new ValidationException('default text not found', $errors);
        }
        if(empty($default_text['name'])){
            $errors['text->name'] = 'Input required';
        }
        if(empty($default_text['price'])){
            $errors['text->price'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    public function updateOrder(int $id,int $order_item = 0){
        $this->repository->updateOrder($id,$order_item);
    }

    /** All method for web */
    public function get_active_video(){
        $data = $this->repository->get_active_video();
        if(empty($data) || is_null($data))
            $data = [];
        foreach($data as $key => $val){
            $data[$key]['texts'] = $this->textRepository->getByVideoId($val['id']);
        }

        return $data;
    }
}