<?php
namespace App\Domain\Video\Data;
final class VideoData
{
    public $id;
    public $coverUrl;
    public $thumUrl;
    public $stream_preview;
    public $stream_full;
    public $link;
    public $year;
    public $isActive;
    public $texts;
}