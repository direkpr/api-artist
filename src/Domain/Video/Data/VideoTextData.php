<?php
namespace App\Domain\Video\Data;
final class VideoTextData
{
    public $id;
    public $video_id;
    public $name;
    public $price;
    public $lg;
}