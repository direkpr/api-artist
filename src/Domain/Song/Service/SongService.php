<?php

namespace App\Domain\Song\Service;
use App\Factory\LoggerFactory;
use App\Domain\Song\Data\SongData;
use App\Domain\Song\Repository\SongRepository;
use App\Domain\Song\Repository\SongTextRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;
use App\Domain\Album\Repository\AlbumRepository;
use App\Domain\Album\Repository\AlbumTextRepository;

/**
 * Service.
 */
final class SongService
{

    private $repository;
    private $textRepository;
    private $langsRepository;
    private $logger;
    private $albumRepository;
    private $albumTextRepository;

    public function __construct(
        SongRepository $repository,
        SongTextRepository $textRepository,
        AlbumRepository $albumRepository,
        AlbumTextRepository $albumTextRepository,
        LangsRepository $langsRepository, 
        LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->textRepository = $textRepository;
        $this->albumRepository = $albumRepository;
        $this->albumTextRepository = $albumTextRepository;
        $this->langsRepository = $langsRepository;

        $this->logger = $logger
            ->addFileHandler('SongService.log')
            ->createInstance('SongService');
    }
    public function deleteSong(int $id){
        $this->repository->delete($id);
        $this->textRepository->deleteBySongId($id);
    }
    public function getById(int $id) : SongData {
        $data = $this->repository->getById($id);
        $return = new SongData();
        $return->id = $data['id'];
        $return->album_id = $data['album_id'];
        $return->stream_preview = $data['stream_preview'];
        $return->stream_full = $data['stream_full'];
        $return->link = $data['link'];
        $return->duration = $data['duration'];
        $return->isActive = $data['isActive'];
        $return->texts = $this->textRepository->getBySongId((int)$data['id']);
        return $return;
    }
    public function createSong(array $data): int
    {
        // Input validation
        $this->validateNewSong($data);
        $datasearch = '';
        foreach($data['texts'] as $text){
            $datasearch .= $text['name'].$text['artist'].$text['price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            foreach($data['texts'] as $text){
                $text['song_id'] = $insert_id;
                if($this->validateTextSong($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
        return $insert_id;
    }
    public function updateSong(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "album_id"=>$olddata->album_id,
            "stream_preview"=>$olddata->stream_preview,
            "stream_full"=>$olddata->stream_full,
            "link"=>$olddata->link,
            "duration"=>$olddata->duration,
            "isActive"=>$olddata->isActive,
            "texts"=>$olddata->texts
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        //$this->logger->info(print_r($oldarray,true));
        $this->validateNewSong($oldarray);
        $datasearch = '';
        foreach($oldarray['texts'] as $text){
            $datasearch .= $text['name'].$text['artist'].$text['price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $oldarray['datasearch'] = $datasearch;
        $this->repository->update($id,$oldarray);
        foreach($oldarray['texts'] as $text){
            if(isset($text['id'])){
                //update
                $text['song_id'] = $id;
                if($this->validateTextSong($text)){
                    $this->textRepository->update($text['id'],$text);
                }
            }else{
                $text['song_id'] = $id;
                if($this->validateTextSong($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $lang = $this->langsRepository->get_default_lang();
        $params['lg'] = $lang->code;
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    private function validateTextSong(array $data): bool {
        if(empty($data['lg'])){
            return false;
        }
        if(empty($data['name'])){
            return false;
        }
        if(empty($data['artist'])){
            return false;
        }
        if(empty($data['price'])){
            return false;
        }
        return true;
    }

    private function validateNewSong(array $data): void
    {
        $errors = [];

        if (empty($data['album_id'])) {
            $errors['album_id'] = 'Input required';
        }

        if (empty($data['stream_preview'])) {
            $errors['stream_preview'] = 'Input required';
        }

        if (empty($data['stream_full'])) {
            $errors['stream_full'] = 'Input required';
        }

        if (empty($data['link'])) {
            $errors['link'] = 'Input required';
        }

        if (empty($data['duration'])) {
            $errors['duration'] = 'Input required';
        }

        if(empty($data['texts'])){
            $errors['texts'] = 'Input required';
        }

        $lang = $this->langsRepository->get_default_lang();
        if(!$lang){
            $errors['lang'] = "default lang not found";
            throw new ValidationException('default lang not found', $errors);
        }
        //var_dump($lang['code']);
        $default_text = null;
        foreach($data['texts'] as $text){
            if($lang['code'] == $text['lg']){
                $default_text = $text;
            }
        }
        //var_dump($default_text);
        if(is_null($default_text)){
            $errors['texts'] = 'default text not found';
            throw new ValidationException('default text not found', $errors);
        }
        if(empty($default_text['name'])){
            $errors['text->name'] = 'Input required';
        }
        if(empty($default_text['artist'])){
            $errors['text->artist'] = 'Input required';
        }
        if(empty($default_text['price'])){
            $errors['text->price'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    public function updateSongOrder(int $id,int $order_item = 0){
        $this->repository->updateSongOrder($id,$order_item);
    }


    /** All method for web */
    public function get_active_song(){
        $albums = $this->albumRepository->get_active_album();
        
        $data = $this->repository->get_active_song();
        if(empty($data) || is_null($data))
            $data = [];
        foreach($data as $key => $val){
            $data[$key]['texts'] = $this->textRepository->getBySongId($val['id']);
        }
        foreach($albums as $key => $val){
            $albums[$key]['texts'] = $this->albumTextRepository->getByAlbumId($val['id']);
            $albums[$key]['songs'] = [];
            $songs = [];
            foreach($data as $k => $v){
                if($val['id'] == $v['album_id']){
                    array_push($songs,$v);
                }
            }
            $albums[$key]['songs'] = $songs;
        }

        return $albums;
    }
}