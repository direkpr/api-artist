<?php
namespace App\Domain\Song\Data;
final class SongData
{
    public $id;
    public $album_id;
    public $stream_preview;
    public $stream_full;
    public $link;
    public $duration;
    public $isActive;
    public $texts;
}