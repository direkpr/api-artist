<?php
namespace App\Domain\Song\Data;
final class SongTextData
{
    public $id;
    public $song_id;
    public $name;
    public $artist;
    public $price;
    public $lg;
}