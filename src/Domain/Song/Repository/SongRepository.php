<?php
namespace App\Domain\Song\Repository;
use PDO;

class SongRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'album_id' => ($data['album_id']) ? $data['album_id'] : -1,
            'stream_preview' => ($data['stream_preview']) ? $data['stream_preview'] : "",
            'stream_full' => ($data['stream_full']) ? $data['stream_full'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'duration' => ($data['duration']) ? $data['duration'] : 0,
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO songs SET 
                album_id=:album_id, 
                stream_preview=:stream_preview, 
                stream_full=:stream_full, 
                link=:link, 
                duration=:duration, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function updateSongOrder($id,$order_item){
        $row = [
            'id' => $id,
            'order_item' => ($order_item) ? $order_item : 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE songs SET 
                order_item=:order_item, 
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'album_id' => ($data['album_id']) ? $data['album_id'] : -1,
            'stream_preview' => ($data['stream_preview']) ? $data['stream_preview'] : "",
            'stream_full' => ($data['stream_full']) ? $data['stream_full'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'duration' => ($data['duration']) ? $data['duration'] : 0,
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE songs SET 
                album_id=:album_id, 
                stream_preview=:stream_preview, 
                stream_full=:stream_full, 
                link=:link, 
                duration=:duration, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function get_total_count($search) : int 
    {
        $datasearch = $search;
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;

        $sql = "select count(*) as num from songs where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $datasearch = $param['search'];
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
            $where = "where a.datasearch like '%{$search}%'";
        }
        //$someArray = json_decode($param['where']);

        //error_log($someArray->album_id );
        if(isset($param['where'])){
            $json_where = json_decode($param['where']);
            
            $where_arr = [];
            foreach($json_where as $key => $value){
                array_push($where_arr,"a.".$key."=".$value);
            }
            $where_str = join(" and ",$where_arr);
            //error_log( $where_str );
            if(strlen($where) > 0){
                $where = $where." and ".$where_str;
            }else{
                $where = "where ".$where_str;
            }
        }

        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,c.title as album,bb.year as year,b.name,b.artist,b.price,a.stream_preview,a.stream_full,a.link,a.duration,a.createdtime,a.updatedtime,a.isActive,a.order_item FROM `songs` a left JOIN songs_txt b on a.id = b.song_id and b.lg = '{$lg}' left join albums bb on a.album_id = bb.id left join albums_txt c on a.album_id = c.album_id and c.lg = '{$lg}' {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,album_id,stream_preview,stream_full,link,duration,isActive from songs where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Goods not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `songs` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /** method for front-end */
    public function get_active_song(){
        $sql = "select id,album_id,stream_preview,stream_full,link,duration,isActive,order_item from songs where isActive = 1 order by album_id desc, order_item asc";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
}
