<?php
namespace App\Domain\Song\Repository;
use PDO;

class SongTextRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'song_id' => $data['song_id'],
            'name' => ($data['name']) ? $data['name'] : "",
            'artist' => ($data['artist']) ? $data['artist'] : "",
            'price' => ($data['price']) ? $data['price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "INSERT INTO songs_txt SET 
                song_id=:song_id, 
                name=:name, 
                artist=:artist, 
                price=:price, 
                lg=:lg";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'song_id' => $data['song_id'],
            'name' => ($data['name']) ? $data['name'] : "",
            'artist' => ($data['artist']) ? $data['artist'] : "",
            'price' => ($data['price']) ? $data['price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "UPDATE songs_txt SET 
                song_id=:song_id, 
                name=:name, 
                artist=:artist, 
                price=:price, 
                lg=:lg WHERE id =:id";
        $this->connection->prepare($sql)->execute($row);
    }
    public function getBySongId(int $id) {
        $sql = "select id,song_id,name,artist,price,lg from songs_txt where song_id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        if (!$rows) {
            $rows = [];
        }
        return $rows;
    }
    public function deleteBySongId(int $id){
        $sql = "DELETE FROM `songs_txt` WHERE `song_id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
