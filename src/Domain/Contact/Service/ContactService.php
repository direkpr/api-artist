<?php

namespace App\Domain\Contact\Service;
use App\Factory\LoggerFactory;
use App\Domain\Contact\Data\ContactData;
use App\Domain\Contact\Repository\ContactRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
use Slim\App;

/**
 * Service.
 */
final class ContactService
{

    private $repository;
    private $logger;
    private $app;

    public function __construct(ContactRepository $repository,LoggerFactory $logger,App $app)
    {
        $this->repository = $repository;
        $this->logger = $logger
            ->addFileHandler('ContactService.log')
            ->createInstance('ContactService');
        $this->app = $app;
    }
    public function deleteContact(int $id){
        $this->repository->delete($id);
    }
    public function getById(int $id) : ContactData {
        $data = $this->repository->getById($id);
        $return = new ContactData();
        $return->id = $data['id'];
        $return->fname = $data['fname'];
        $return->lname = $data['lname'];
        $return->email = $data['email'];
        $return->mobile = $data['mobile'];
        $return->company = $data['company'];
        $return->country = $data['country'];
        $return->street1 = $data['street1'];
        $return->street2 = $data['street2'];
        $return->city = $data['city'];
        $return->state = $data['state'];
        $return->zipcode = $data['zipcode'];
        $return->subject = $data['subject'];
        $return->message = $data['message'];
        return $return;
    }
    public function createContact(array $data): int
    {
        // Input validation
        $this->validateNewContact($data);
        $datasearch = $data['fname'].$data['lname'].$data['email'].$data['mobile'].$data['company'].$data['subject'];
        
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            $this->sendmail($data);
        }
        return $insert_id;
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    private function validateNewContact(array $data): void
    {
        $errors = [];

        if (empty($data['fname'])) {
            $errors['fname'] = 'Input required';
        }

        if (empty($data['lname'])) {
            $errors['lname'] = 'Input required';
        }

        if (empty($data['email'])) {
            $errors['email'] = 'Input required';
        }

        if (empty($data['mobile'])) {
            $errors['mobile'] = 'Input required';
        }

        if (empty($data['country'])) {
            $errors['country'] = 'Input required';
        }
        if (empty($data['street1'])) {
            $errors['street1'] = 'Input required';
        }

        if (empty($data['city'])) {
            $errors['city'] = 'Input required';
        }
        if (empty($data['state'])) {
            $errors['state'] = 'Input required';
        }
        if (empty($data['zipcode'])) {
            $errors['zipcode'] = 'Input required';
        }
        if (empty($data['subject'])) {
            $errors['subject'] = 'Input required';
        }
        if (empty($data['message'])) {
            $errors['message'] = 'Input required';
        }
        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }
    private function sendmail($data){
$name = $data['fname'].' '.$data['lname'];
$email = $data['email'];
$mobile = $data['mobile'];
$company = (isset($data['company'])) ? $data['company'] : "-";
$address1 = $data['street1'];
$address2 = (isset($data['street2'])) ? $data['street2'] : "";
$city = $data['city'];
$state = $data['state'];
$zipcode = $data['zipcode'];
$country = $data['country'];
$msg = $data['message'];

$message = <<<EOD
<dl>
  <dt style="float: left;clear: left;width: 180px;"><b>NAME</b></dt>
  <dd>{$name}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>EMAIL ADDRESS</b></dt>
  <dd>{$email}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>MOBILE NUMBER</b></dt>
  <dd>{$mobile}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>COMPANY NAME</b></dt>
  <dd>{$company}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>STREET ADDRESS</b></dt>
  <dd>{$address1}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>&nbsp;</b></dt>
  <dd>{$address2}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>CITY</b></dt>
  <dd>{$city}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>STATE/PROVICE/REGION</b></dt>
  <dd>{$state}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>ZIPCODE</b></dt>
  <dd>{$zipcode}</dd>
  <dt style="float: left;clear: left;width: 180px;"><b>COUNTRY</b></dt>
  <dd>{$country}</dd>
</dl>
<div>
  <dl>
    <dt style="float: left;clear: left;width: 180px;"><b>MESSAGE</b></dt>
    <dd>{$msg}</dd>
  </dl>
</div>
EOD;
        $container = $this->app->getContainer();
        //try{
            $client = new PostmarkClient($container->get('settings')['postmark_token']);
            $sendResult = $client->sendEmail($container->get('settings')['postmark_sender'], 
                "webdev@eqho.com", 
                "Realme Website:".$data['subject'],
                $message);
        //}catch(PostmarkException $ex){//natthanan.w@eqho.com,
            // If client is able to communicate with the API in a timely fashion,
            // but the message data is invalid, or there's a server error,
            // a PostmarkException can be thrown.
            //echo $ex->httpStatusCode;
            //echo $ex->message;
            //echo $ex->postmarkApiErrorCode;
        
        //}catch(Exception $generalException){
            // A general exception is thrown if the API
            // was unreachable or times out.
        //}
    }
}