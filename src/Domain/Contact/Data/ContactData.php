<?php
namespace App\Domain\Contact\Data;
final class ContactData
{
    public $id;
    public $fname;
    public $lname;
    public $email;
    public $mobile;
    public $company;
    public $country;
    public $street1;
    public $street2;
    public $city;
    public $state;
    public $zipcode;
    public $subject;
    public $message;
    public $createdtime;
}