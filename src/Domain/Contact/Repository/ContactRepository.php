<?php
namespace App\Domain\Contact\Repository;
use PDO;

class ContactRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'fname' => ($data['fname']) ? $data['fname'] : "",
            'lname' => ($data['lname']) ? $data['lname'] : "",
            'email' => ($data['email']) ? $data['email'] : "",
            'mobile' => ($data['mobile']) ? $data['mobile'] : "",
            'company' => ($data['company']) ? $data['company'] : "",
            'country' => ($data['country']) ? $data['country'] : "",
            'street1' => ($data['street1']) ? $data['street1'] : "",
            'street2' => ($data['street2']) ? $data['street2'] : "",
            'city' => ($data['city']) ? $data['city'] : "",
            'state' => ($data['state']) ? $data['state'] : "",
            'zipcode' => ($data['zipcode']) ? $data['zipcode'] : "",
            'subject' => ($data['subject']) ? $data['subject'] : "",
            'message' => ($data['message']) ? $data['message'] : "",
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'createdtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO contact SET 
                fname=:fname, 
                lname=:lname, 
                email=:email, 
                mobile=:mobile, 
                company=:company, 
                country=:country, 
                street1=:street1, 
                street2=:street2, 
                city=:city, 
                state=:state, 
                zipcode=:zipcode, 
                subject=:subject, 
                message=:message, 
                datasearch=:datasearch,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function get_total_count($search) : int 
    {
        $sql = "select count(*) as num from contact where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $search = $param['search'];
            $where = "where a.datasearch like '%{$search}%'";
        }
        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,a.fname, a.lname,a.email,a.mobile,a.company,a.country,a.street1,a.street2,a.city,a.state,a.zipcode,a.subject,a.message,a.createdtime FROM `contact` a {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,fname,lname,email,mobile,company,country,street1,street2,city,state,zipcode,subject,message from contact where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Contact not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `contact` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
