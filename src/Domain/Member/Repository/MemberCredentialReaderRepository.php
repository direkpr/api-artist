<?php

namespace App\Domain\Member\Repository;

use App\Domain\Member\Data\MemberCredentialData;
use DomainException;
use PDO;

/**
 * Repository.
 */
class MemberCredentialReaderRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getCredentialByMemberId(int $memberId): MemberCredentialData
    {
        $sql = "SELECT id, memberId,password FROM member_credential WHERE memberId = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $memberId]);

        $row = $statement->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Member credential not found: %s', $userId));
        }

        // Map array to data object
        $user = new MemberCredentialData();
        $user->id = (int)$row['id'];
        $user->memberId = (int)$row['memberId'];
        $user->password = (string)$row['password'];
        return $user;
    }
}