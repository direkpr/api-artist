<?php

namespace App\Domain\Member\Repository;

use PDO;

/**
 * Repository.
 */
class MemberCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `member` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
    public function del_member_social($member_id){
        $sql = "DELETE FROM `member_social` WHERE member_id = {$member_id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
    public function get_member_social_by_memberId($member_id){
        $sql = "SELECT * FROM `member_social` where member_id = '{$member_id}'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return $row;
    }
    public function get_member_social($social_id){
        $sql = "SELECT * FROM `member_social` where social_id = '{$social_id}'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return $row;
    }
    public function update_member_social($data){
        $row = [
            'member_id' => $data['member_id'],
            'social_id' => $data['social_id'],
            'provider' => $data['provider'],
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE member_social SET 
                member_id=:member_id, 
                provider=:provider, 
                updatedtime=:updatedtime
                WHERE social_id=:social_id";

        return $this->connection->prepare($sql)->execute($row);
    }
    public function add_member_social($data){
        $row = [
            'member_id' => ($data['member_id']) ? $data['member_id'] : "",
            'social_id' => ($data['social_id']) ? $data['social_id'] : "",
            'provider' => ($data['provider']) ? $data['provider'] : "",
            'createdtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO member_social SET 
                member_id=:member_id, 
                social_id=:social_id, 
                provider=:provider, 
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }

    /**
     * Insert member row.
     *
     * @param array $member The member
     *
     * @return int The new ID
     */
    public function insertMember(array $member): int
    {
        $row = [
            'mobile' => ($member['mobile']) ? $member['mobile'] : "",
            'firstname' => ($member['firstname']) ? $member['firstname'] : "",
            'lastname' => ($member['lastname']) ? $member['lastname'] : "",
            'email' => $member['email'],
            'birthday' => ($member['birthday']) ? $member['birthday'] : null,
            'roles' => json_encode(($member['roles']) ? $member['roles'] : ["customer"]),
            'payment_status'=>($member['payment_status']) ? $member['payment_status'] : 0,
            'use_cms'=>($member['use_cms']) ? $member['use_cms'] : 0,
            'createdtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO member SET 
                mobile=:mobile, 
                firstname=:firstname, 
                lastname=:lastname, 
                birthday=:birthday, 
                roles=:roles, 
                createdtime=:createdtime,
                payment_status=:payment_status,
                use_cms=:use_cms,
                email=:email;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function updateMemberByEmail(array $member){
        $row = [
            'mobile' => ($member['mobile']) ? $member['mobile'] : "",
            'firstname' => ($member['firstname']) ? $member['firstname'] : "",
            'lastname' => ($member['lastname']) ? $member['lastname'] : "",
            'email' => $member['email'],
            'birthday' => ($member['birthday']) ? $member['birthday'] : null,
            'roles' => json_encode(($member['roles']) ? $member['roles'] : ["customer"]),
            'payment_status'=>($member['payment_status']) ? $member['payment_status'] : 0,
            'use_cms'=>($member['use_cms']) ? $member['use_cms'] : 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE member SET 
                mobile=:mobile, 
                firstname=:firstname, 
                lastname=:lastname, 
                birthday=:birthday, 
                roles=:roles, 
                updatedtime=:updatedtime,
                payment_status=:payment_status,
                use_cms=:use_cms
                WHERE email=:email;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function updateMemberById($member_id,array $member){
        $row = [
            'id'=>intval($member_id),
            'mobile' => ($member['mobile']) ? $member['mobile'] : "",
            'firstname' => ($member['firstname']) ? $member['firstname'] : "",
            'lastname' => ($member['lastname']) ? $member['lastname'] : "",
            'birthday' => ($member['birthday']) ? $member['birthday'] : null,
            'use_cms'=>($member['use_cms']) ? $member['use_cms'] : 0,
            'payment_status'=>($member['payment_status']) ? $member['payment_status'] : 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE member SET 
                mobile=:mobile, 
                firstname=:firstname, 
                lastname=:lastname, 
                birthday=:birthday, 
                updatedtime=:updatedtime,
                payment_status=:payment_status,
                use_cms=:use_cms
                WHERE id=:id";

        return $this->connection->prepare($sql)->execute($row);
    }
    public function updateAdminMemberById($member_id,array $member){
        $row = [
            'id'=>intval($member_id),
            'mobile' => ($member['mobile']) ? $member['mobile'] : "",
            'firstname' => ($member['firstname']) ? $member['firstname'] : "",
            'lastname' => ($member['lastname']) ? $member['lastname'] : "",
            'birthday' => ($member['birthday']) ? $member['birthday'] : null,
            'use_cms'=>($member['use_cms']) ? $member['use_cms'] : 0,
            'roles'=>($member['roles']) ? json_encode($member['roles']) : json_encode(["guest"]),
            'payment_status'=>($member['payment_status']) ? $member['payment_status'] : 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE member SET 
                mobile=:mobile, 
                firstname=:firstname, 
                lastname=:lastname, 
                birthday=:birthday, 
                updatedtime=:updatedtime,
                payment_status=:payment_status,
                use_cms=:use_cms,
                roles=:roles
                WHERE id=:id";
        return $this->connection->prepare($sql)->execute($row);
    }
    public function setSubscripbeToOneTime($member_id){
        $row2 = [
            'member_id'=>intval($member_id),
            'subscription_type' => 1,
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql2 = "UPDATE customer SET 
                subscription_type=:subscription_type, 
                updatedtime=:updatedtime
                WHERE member_id=:member_id";
        
        return $this->connection->prepare($sql2)->execute($row2);
    }
    public function setGeust($member_id){
        $row = [
            'id'=>intval($member_id),
            'roles' => json_encode(["guest"]),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE member SET 
                roles=:roles, 
                updatedtime=:updatedtime
                WHERE id=:id";
        $this->connection->prepare($sql)->execute($row);

        $row2 = [
            'member_id'=>intval($member_id),
            'subscription_type' => 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql2 = "UPDATE customer SET 
                subscription_type=:subscription_type, 
                updatedtime=:updatedtime
                WHERE member_id=:member_id";
        
        return $this->connection->prepare($sql2)->execute($row2);
    }
}
