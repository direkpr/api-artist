<?php

namespace App\Domain\Member\Repository;

use App\Domain\Member\Data\MemberData;
use DomainException;
use PDO;

/**
 * Repository.
 */
class MemberReaderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get member by the given member id.
     *
     * @param int $userId The member id
     *
     * @throws DomainException
     *
     * @return MemberData The member data
     */
    public function getMemberById(int $userId): MemberData
    {
        $sql = "SELECT id, mobile, firstname, lastname, email,birthday,roles,displayname,payment_status FROM member WHERE id = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $userId]);

        $row = $statement->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Member not found: %s', $userId));
        }

        // Map array to data object
        $user = new MemberData();
        $user->id = (int)$row['id'];
        $user->mobile = (string)$row['mobile'];
        $user->firstname = (string)$row['firstname'];
        $user->lastname = (string)$row['lastname'];
        $user->email = (string)$row['email'];
        $user->birthday = (string)$row['birthday'];
        $user->displayname = (string)$row['displayname'];
        $user->roles = json_decode((string)$row['roles']);
        $user->payment_status = (int)$row['payment_status'];

        return $user;
    }
    public function setSubscripbeToOneTime($memberId){
        
    }
    public function getRoles(int $userId)
    {
        $sql = "SELECT id, mobile, firstname, lastname, email,birthday,roles,displayname,payment_status FROM member WHERE id = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $userId]);

        $row = $statement->fetch();
        return json_decode((string)$row['roles']);
    }

    public function getMemberByEmail(string $email) : MemberData {
        $sql = "SELECT id, mobile, firstname, lastname, email,birthday,roles,displayname,payment_status FROM member WHERE email = :email;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['email' => $email]);

        $row = $statement->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Member not found: %s', $userId));
        }

        // Map array to data object
        $user = new MemberData();
        $user->id = (int)$row['id'];
        $user->mobile = (string)$row['mobile'];
        $user->firstname = (string)$row['firstname'];
        $user->lastname = (string)$row['lastname'];
        $user->email = (string)$row['email'];
        $user->birthday = (string)$row['birthday'];
        $user->displayname = (string)$row['displayname'];
        $user->roles = json_decode((string)$row['roles']);
        $user->payment_status = (int)$row['payment_status'];

        return $user;
    }
    public function getCustomer(int $member_id){
        $sql = "SELECT * FROM customer WHERE member_id = :member_id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['member_id' => $member_id]);

        $row = $statement->fetch();
        return $row;
    }
    public function getLastPurchase(int $member_id){
        $sql = "SELECT * FROM purchases WHERE member_id = :member_id order by createdtime desc limit 1;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['member_id' => $member_id]);

        $row = $statement->fetch();
        return $row;
    }
    public function gen_link_reset($member_id){
        //reset_password
        $row = array(
            "member_id"=>$member_id,
            "token"=>md5($member_id.time()),
            "isUse"=>0,
            "createdtime"=>date("Y-m-d H:i:s")
        );
        $sql = "INSERT INTO reset_password SET 
                member_id=:member_id, 
                token=:token, 
                isUse=:isUse, 
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);
        return $row['token'];
    }
    public function check_link_reset($token){
        $sql = "SELECT * FROM reset_password WHERE token = :token and isUse = 0;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['token' => $token]);
        $row = $statement->fetch();
        return $row;
    }
    public function set_used_link_reset($token){
        $row = [
            'token'=>$token,
            'isUse' => 1,
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE reset_password SET 
                isUse=:isUse, 
                updatedtime=:updatedtime
                WHERE token=:token";

        return $this->connection->prepare($sql)->execute($row);
    }
    public function getAllCMSUser($params){

        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $datasearch = $param['search'];
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
            $where = "where use_cms = 1 and ( email like '%%{$search}%%' or firstname like '%%{$search}%%' or lastname like '%%{$search}%%')";
        }else{
            $where = "where use_cms = 1";
        }
        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT * FROM `member` {$where} {$orderby} {$limit}";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function get_admin_total_count($sarch){
        $datasearch = $search;
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
        $sql = "select count(*) as num from member where email like '%%{$search}%%' or firstname like '%%{$search}%%' or lastname like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);

        return 0;
    }
    public function getAllCustomer(){
        $sql = "SELECT a.member_id,a.subscription_type,a.expired,a.next_billdate,a.createdtime,b.firstname,b.lastname,b.birthday,b.email,b.mobile FROM `customer` a LEFT join member b on a.member_id = b.id";
        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getRecurringAllCustomer(){
        $sql = "SELECT a.id, a.invoice, a.member_id, a.amount, a.currency, a.status, a.createdtime, a.isRenew, a.subscription_type, b.firstname, b.lastname, b.email, b.mobile, b.birthday FROM `purchases` a LEFT JOIN member b ON a.member_id = b.id WHERE b.id > 0 AND a.subscription_type = 2";
        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $rows = $statement->fetchAll();
        return $rows;
    }

    public function getRenewAllCustomer(){
        $sql = "SELECT a.id, a.invoice, a.member_id, a.amount, a.currency, a.status, a.createdtime, a.isRenew, a.subscription_type, b.firstname, b.lastname, b.email, b.mobile, b.birthday FROM `purchases` a LEFT JOIN member b ON a.member_id = b.id WHERE b.id > 0 AND a.subscription_type = 1";
        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getTodayBirthdayMembers(){
        $date = date("-m-d");
        $sql = "SELECT * FROM `member` where payment_status = 1 and birthday like '%".$date."'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getEBirthdaySendAll(){
        $sql = "SELECT a.*,b.firstname,b.lastname,b.email,b.mobile FROM `ebirthday_sends` a LEFT JOIN member b on a.member_id = b.id ";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getAllActiveCards(){
        $sql = "SELECT * FROM `ebirthday_cards` WHERE isActive = 1";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function log_send_ecard($member){
        $row = [
            'member_id' => ($member['id']) ? $member['id'] : "",
            'birthday' => ($member['birthday']) ? $member['birthday'] : "",
            'createdtime' => date("Y-m-d H:i:s")
        ];
        $sql = "INSERT INTO ebirthday_sends SET 
                member_id=:member_id, 
                birthday=:birthday, 
                createdtime=:createdtime;";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
}