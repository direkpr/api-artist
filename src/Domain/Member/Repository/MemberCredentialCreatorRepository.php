<?php

namespace App\Domain\Member\Repository;

use PDO;

/**
 * Repository.
 */
class MemberCredentialCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function deleteByMemberId(int $id){
        $sql = "DELETE FROM `member_credential` WHERE `memberId` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /**
     * Insert member credential row.
     *
     * @param array $member The member credential
     *
     * @return int The new ID
     */
    public function insertMemberCredential(array $member): int
    {
        $row = [
            'memberId' => $member['memberId'],
            'password' => md5($member['password']),
            'createdtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO member_credential SET 
                memberId=:memberId, 
                password=:password, 
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function updateMemberCredentialById($member_id,$password){
        $row = [
            'memberId' => $member_id,
            'password' => md5($password),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE member_credential SET 
                password=:password, 
                updatedtime=:updatedtime where memberId=:memberId;";

        $this->connection->prepare($sql)->execute($row);
    }
}
