<?php

namespace App\Domain\Member\Service;

use App\Domain\Member\Data\MemberCredentialData;
use App\Domain\Member\Repository\MemberCredentialReaderRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class MemberCredentialReader
{
    private $repository;
    public function __construct(MemberCredentialReaderRepository $repository)
    {
        $this->repository = $repository;
    }
    public function getCredentialByMemberId(int $memberId): MemberCredentialData
    {
        // Validation
        if (empty($memberId)) {
            throw new ValidationException('Member ID required');
        }
        $user = $this->repository->getCredentialByMemberId($memberId);
        return $user;
    }
}