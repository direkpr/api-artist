<?php

namespace App\Domain\Member\Service;

use App\Domain\Omise\Service\Customer;
use App\Domain\Member\Repository\MemberCreatorRepository;
use App\Domain\Member\Repository\MemberReaderRepository;
use App\Domain\Member\Repository\MemberCredentialCreatorRepository;
use App\Exception\ValidationException;
use DomainException;
/**
 * Service.
 */
final class MemberCreator
{
    /**
     * @var MemberCreatorRepository
     */
    private $repository;
    private $credentialRepository;
    private $readerRepository;
    private $customer;
    /**
     * The constructor.
     *
     * @param MemberCreatorRepository $repository The repository
     */
    public function __construct(MemberCreatorRepository $repository,MemberCredentialCreatorRepository $credentialRepository,MemberReaderRepository $readerRepository,Customer $customer)
    {
        $this->repository = $repository;
        $this->credentialRepository = $credentialRepository;
        $this->readerRepository = $readerRepository;
        $this->customer = $customer;
    }
    public function delete(int $id){
        $this->repository->delete($id);
        $this->credentialRepository->deleteByMemberId($id);
        return true;
    }
    public function checkSocialConnect($data){
        if(!isset($data['member_id'])){
            return array("status"=>false,"message"=>"member_id not found!");
        }
        $social = $this->repository->get_member_social_by_memberId($data['member_id']);
        if($social && isset($social['social_id'])){
            return array(
                "status"=>true,
                "social"=>$social
            );
        }
        return array("status"=>false,"message"=>"member not link!");
    }
    public function SocialLogin(array $data){
        if(!isset($data['social_id'])){
            return false;
        }
        $social = $this->repository->get_member_social($data['social_id']);
        if($social && isset($social['social_id'])){
            return $social;
        }
        return false;
    }
    public function SocialDisconnect(array $data){
        if(!isset($data['member_id'])){
            return array(
                "status"=>false,
                "message"=>"member_id not found!"
            );
        }
        $this->repository->del_member_social($data['member_id']);
        return array(
            "status"=>true,
            "message"=>"Disconnect social network successful"
        );
    }
    public function SocialConnect(array $data){
        if(!isset($data['member_id'])){
            return array(
                "status"=>false,
                "message"=>"member_id not found!"
            );
        }
        if(!isset($data['social_id'])){
            return array(
                "status"=>false,
                "message"=>"social_id not found!"
            );
        }
        $social = $this->repository->get_member_social($data['social_id']);
        if($social && isset($social['social_id'])){
            if($social['member_id'] != $data['member_id']){
                //update member data
                if($this->repository->update_member_social($data)){
                    return array(
                        "status"=>true,
                        "message"=>"Connect social network successful"
                    );
                }else{
                    return array(
                        "status"=>false,
                        "message"=>"Connect social network fail"
                    );
                }
            }
            return array(
                "status"=>true,
                "message"=>"Connect social network successful"
            );
        }else{
            //add new data
            $id = $this->repository->add_member_social($data);
            if($id){
                return array(
                    "status"=>true,
                    "message"=>"Connect social network successful"
                ); 
            }else{
                return array(
                    "status"=>false,
                    "message"=>"Connect social network fail"
                );
            }
        }
    }

    public function renewMember(array $data){
        if(!isset($data['memberId'])){
            return array(
                "status"=>false,
                "message"=>"Member data not found!"
            );
        }
        if(!isset($data['subscription_type'])){
            return array(
                "status"=>false,
                "message"=>"Subscribtion Type not found!"
            );
        }

        $member = $this->readerRepository->getMemberById($data['memberId']);
        $customer = $this->readerRepository->getCustomer($data['memberId']);
        //generate invoice for purchase
        $invoice = $this->customer->createInvoice($member->id,($data['subscription_type'] == 2) ? MONTHLY_PURCHASE_AMT : ONETIME_PURCHASE_AMT,CURRENCY_PURCHASE,1,$data['subscription_type']);
            
        // Transform the result into the JSON representation
        $result = [
            "status"=>true,
            "message"=>"save data successful",
            'memberId' => $member->id,
            'customer'=>$customer,
            'invoice'=>$invoice
        ];
        return $result;

    }
    public function updateMember(int $memberId,array $data){
        if(isset($data['password']) && $data['password'] != "USEOLDUSEOLD"){
            $this->credentialRepository->updateMemberCredentialById($memberId,$data['password']);
        }
        return $this->repository->updateMemberById($memberId,$data);
    }

    public function updateMemberByAdmin(int $memberId,array $data){
        if(isset($data['password']) && $data['password'] != "USEOLDUSEOLD"){
            $this->credentialRepository->updateMemberCredentialById($memberId,$data['password']);
        }
        return $this->repository->updateAdminMemberById($memberId,$data);
    }

    /**
     * Create a new user.
     *
     * @param array $data The form data
     *
     * @return int The new user ID
     */
    public function createMember(array $data): int
    {
        // Input validation
        $this->validateNewMember($data);

        try {
            $check_duplicate_email = $this->readerRepository->getMemberByEmail($data['email']);
            if($check_duplicate_email->id){
                $this->repository->updateMemberByEmail($data);
                return $check_duplicate_email->id;
            }
        }
        catch(DomainException $e){
            // Insert user
            $userId = $this->repository->insertMember($data);
            $data['memberId'] = $userId;
            $this->credentialRepository->insertMemberCredential($data);
            // Logging here: User created successfully
            //$this->logger->info(sprintf('User created successfully: %s', $userId));

            return $userId;
        }
    }

    /**
     * Input validation.
     *
     * @param array $data The form data
     *
     * @throws ValidationException
     *
     * @return void
     */
    private function validateNewMember(array $data): void
    {
        $errors = [];

        // Here you can also use your preferred validation library

        if (empty($data['firstname'])) {
            $errors['firstname'] = 'Input required';
        }

        if (empty($data['password'])) {
            $errors['password'] = 'Input required';
        }

        if (empty($data['email'])) {
            $errors['email'] = 'Input required';
        } elseif (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Invalid email address';
        }
        try {
            $check_duplicate_email = $this->readerRepository->getMemberByEmail($data['email']);
            if($check_duplicate_email->payment_status == 1){
                $errors['email'] = 'Email already in used!';
            }
        }
        catch (DomainException $e){

        }
        

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }
}