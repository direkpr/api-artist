<?php

namespace App\Domain\Member\Service;
use Slim\App;
use App\Domain\Member\Data\MemberData;
use App\Domain\Member\Repository\MemberReaderRepository;
use App\Domain\Member\Repository\MemberCreatorRepository;
use App\Domain\Member\Repository\MemberCredentialCreatorRepository;
use App\Exception\ValidationException;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
use DateTime;
use DomainException;
/**
 * Service.
 */
final class MemberReader
{
    /**
     * @var MemberReaderRepository
     */
    private $repository;
    private $updateRepository;
    private $app;
    private $credentialRepository;
    /**
     * The constructor.
     *
     * @param UserReaderRepository $repository The repository
     */
    public function __construct(MemberReaderRepository $repository,MemberCreatorRepository $updateRepository,App $app,MemberCredentialCreatorRepository $credentialRepository)
    {
        $this->repository = $repository;
        $this->updateRepository = $updateRepository;
        $this->app = $app;
        $this->credentialRepository = $credentialRepository;
    }
    public function send_ebirthday_cards(){
        $container = $this->app->getContainer();
        $members = $this->repository->getTodayBirthdayMembers();
        $cards = $this->repository->getAllActiveCards();
        if($members && count($members) > 0 && count($cards) > 0){
            foreach($members as $member) {
                $idx  = rand(0,count($cards) -1);
                ///get-file-upload?f=
                $this->send_ecard($member,$container->get('settings')['img_link'].str_replace("/get-file-upload?f=","/uploads/",$cards[$idx]['imgUrl']));
                $this->repository->log_send_ecard($member);
            }
        }
        return true;
    }
    public function getEBirthdaySendAll(){
        return $this->repository->getEBirthdaySendAll();
    }
    public function getMemberAlls(){
        return $this->repository->getAllCustomer();
    }
    public function getMemberAdminAlls($params){
        return $this->repository->getAllCMSUser($params);
    }
    public function get_admin_total_count($params) {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_admin_total_count($search);
    }
    public function getMemberRecurringAlls(){
        return $this->repository->getRecurringAllCustomer();
    }

    public function getMemberRenewAlls(){
        return $this->repository->getRenewAllCustomer();
    }

    public function setSubscripbeToOneTime($memberId){
        return $this->updateRepository->setSubscripbeToOneTime($memberId);
    }

    public function getCustomer($memberId){
        return $this->repository->getCustomer($memberId);
    }
    /**
     * Read a member by the given member id.
     *
     * @param int $userId The member id
     *
     * @throws ValidationException
     *
     * @return MemberData The member data
     */
    public function getMemberDetails(int $userId): MemberData
    {
        // Validation
        if (empty($userId)) {
            throw new ValidationException('Member ID required');
        }
        $user = $this->repository->getMemberById($userId);
        $user->customer = $this->repository->getCustomer($userId);
        $user->last_purchase = $this->repository->getLastPurchase($userId);
        return $user;
    }
    public function getMemberById($userId):MemberData
    {
        $user = $this->repository->getMemberById($userId);
        return $user;
    }
    public function getMemberByEmail(string $email):MemberData
    {
        // Validation
        if (empty($email)) {
            throw new ValidationException('Email required');
        }
        $user = $this->repository->getMemberByEmail($email);
        return $user;
    }
    public function updateCustomerStatus($member_id){
        $customer = $this->repository->getCustomer($member_id);
        if($customer){
            $expired = new DateTime($customer['expired']);
            $now = new DateTime();
            if($now > $expired){
                //account expired
                $this->updateRepository->setGeust($member_id);
            }
        }
    }
    public function getRoles($member_id){
        return $this->repository->getRoles($member_id);
    }
    public function changepassword($data){
        $resetData = $this->repository->check_link_reset($data['token']);
        if($resetData && $resetData['isUse'] == 0){
            if(isset($data['password'])){
                $this->credentialRepository->updateMemberCredentialById($resetData['member_id'],$data['password']);
                $this->repository->set_used_link_reset($data['token']);
                return array(
                    "status"=>true,
                    "message"=>"Reset your password successful!"
                );
            }
        }else{
            return array(
                "status"=>false,
                "message"=>"Invalid link, please retry to request reset password again!"
            );
        }
    }
    public function forgotpassword($data){
        if(!isset($data['email'])){
            return array(
                "status"=>false,
                "message"=>"Your email not found!"
            );
        }
        $email = $data['email'];
        try {
            $member = $this->repository->getMemberByEmail($email);
            if($member->payment_status == 1){
                if($this->send_link_forgot($member)){
                    return array(
                        "status"=>true,
                        "message"=>"Please check your email address"
                    );
                }else{
                    return array(
                        "status"=>false,
                        "message"=>"Send email fail, please try again."
                    );
                }
            }else{
                return array(
                    "status"=>false,
                    "message"=>"Your email not found!"
                );
            }
            
        }catch (DomainException $e){
            return array(
                "status"=>false,
                "message"=>"Your email not found!"
            );
        }
        
    }
    
    private function send_link_forgot($member){
        $name = $member->firstname." ".$member->lastname;
        $email = $member->email;
        $mobile = $member->mobile;
        $link = FORGOT_URI.$this->repository->gen_link_reset($member->id);

        $message = <<<EOD
        <div>
        <h4>Dear {$name}</h4>
        <p>Forgot your password?</p>
        </div>
        <p>To reset your password please follow the link below:</p>
        <p><a href="{$link}" target="_blank">{$link}</a></p>
        <div>
        <br />
        <p>Yours sincerely,</p>
        <p>AYANA MIYAKE<br />Realme Co.,Ltd.<br />www.realme.co.jp</p>
        </div>
EOD;
        $container = $this->app->getContainer();
        $client = new PostmarkClient($container->get('settings')['postmark_token']);
        $sendResult = $client->sendEmail(
            $container->get('settings')['postmark_sender'],
            $email,
            "REALME : Forgot your password?",
            $message
        );
        return $sendResult;
        $this->logger->info(print_r($sendResult,true));
    }
    private function send_ecard($member,$cardUrl){
        //var_dump($member);
        $name = $member['firstname']." ".$member['lastname'];
        $email = $member['email'];
        $mobile = $member['mobile'];

        $message = <<<EOD
        <div>
        <p>Thank you for always being together. Have a wonderful, happy, healthy birthday now and forever. Happy Birthday!</p>
        </div>
        <p><img width="500" src="{$cardUrl}" /></p>
        <div>
        <br />
        <p>With love,</p>
        <p>AYANA MIYAKE<br />Realme Co.,Ltd.<br />www.realme.co.jp</p>
        </div>
EOD;
        $container = $this->app->getContainer();
        $client = new PostmarkClient($container->get('settings')['postmark_token']);
        $sendResult = $client->sendEmail(
            $container->get('settings')['postmark_sender'],
            $email,
            "Happy Birthday to ".$name,
            $message
        );
        return $sendResult;
        $this->logger->info(print_r($sendResult,true));
    }
}