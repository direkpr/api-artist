<?php

namespace App\Domain\Member\Data;
final class MemberCredentialData
{
    public $id;
    public $memberId;
    public $password;
}