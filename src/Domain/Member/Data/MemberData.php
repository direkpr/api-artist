<?php

namespace App\Domain\Member\Data;
final class MemberData
{
    /**
     * @var int
     */
    public $id;

    /** @var string */
    public $firstname;

    /** @var string */
    public $lastname;

    /** @var string */
    public $mobile;

    /** @var string */
    public $email;

    /** @var string */
    public $birthday;

    /** @var string */
    public $displayname;

    /** @var string[] */
    public $roles;

    public $payment_status;

    public $customer;
    public $last_purchase;
}