<?php

namespace App\Domain\MarketingScript\Service;
use App\Factory\LoggerFactory;
use App\Domain\MarketingScript\Data\MarketingScriptData;
use App\Domain\MarketingScript\Repository\MarketingScriptRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class MarketingScriptService
{

    private $repository;
    private $logger;

    public function __construct(MarketingScriptRepository $repository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger
            ->addFileHandler('MarketingScriptService.log')
            ->createInstance('MarketingScriptService');
    }
    public function getById(int $id) : MarketingScriptData {
        $data = $this->repository->getById($id);
        $return = new MarketingScriptData();
        $return->id = $data['id'];
        $return->in_head_tag = $data['in_head_tag'];
        $return->open_body_tag = $data['open_body_tag'];
        $return->close_body_tag = $data['close_body_tag'];
        return $return;
    }

    public function updateMarketingScript(int $id,array $data){
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "in_head_tag"=>$olddata->in_head_tag,
            "open_body_tag"=>$olddata->open_body_tag,
            "close_body_tag"=>$olddata->close_body_tag
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        $this->repository->update($id,$oldarray);
    }
    
}