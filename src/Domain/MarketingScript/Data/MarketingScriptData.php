<?php
namespace App\Domain\MarketingScript\Data;
final class MarketingScriptData
{
    public $id;
    public $in_head_tag;
    public $open_body_tag;
    public $close_body_tag;
}