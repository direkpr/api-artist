<?php
namespace App\Domain\MarketingScript\Repository;
use PDO;
use DomainException;

class MarketingScriptRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'in_head_tag' => ($data['in_head_tag']) ? $data['in_head_tag'] : "",
            'open_body_tag' => ($data['open_body_tag']) ? $data['open_body_tag'] : "",
            'close_body_tag' => ($data['close_body_tag']) ? $data['close_body_tag'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE marketing_scripts SET 
                in_head_tag=:in_head_tag, 
                open_body_tag=:open_body_tag, 
                close_body_tag=:close_body_tag, 
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function getById(int $id){
        $sql = "select id,in_head_tag,open_body_tag,close_body_tag from marketing_scripts where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('MarketingScript not found: %s', $id));
        }
        return $row;
    }
}
