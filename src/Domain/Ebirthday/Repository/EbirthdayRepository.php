<?php
namespace App\Domain\Ebirthday\Repository;
use PDO;

class EbirthdayRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'imgUrl' => ($data['imgUrl']) ? $data['imgUrl'] : "",
            'title' => ($data['title']) ? $data['title'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO ebirthday_cards SET 
                imgUrl=:imgUrl, 
                title=:title, 
                isActive=:isActive, 
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'imgUrl' => ($data['imgUrl']) ? $data['imgUrl'] : "",
            'title' => ($data['title']) ? $data['title'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE ebirthday_cards SET 
                imgUrl=:imgUrl, 
                title=:title, 
                isActive=:isActive, 
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    
    public function get_data(){
        $sql = "select * from ebirthday_cards ";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select * from ebirthday_cards where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Card not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `ebirthday_cards` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
