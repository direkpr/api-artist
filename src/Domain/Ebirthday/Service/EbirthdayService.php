<?php

namespace App\Domain\Ebirthday\Service;
use App\Factory\LoggerFactory;
use App\Domain\Ebirthday\Data\EbirthdayData;
use App\Domain\Ebirthday\Repository\EbirthdayRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class EbirthdayService
{
    private $repository;
    private $logger;
    public function __construct(EbirthdayRepository $repository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger
            ->addFileHandler('EbirthdayService.log')
            ->createInstance('EbirthdayService');
    }
    public function deleteEbirthday(int $id){
        $this->repository->delete($id);
    }
    public function getById(int $id) : EbirthdayData {
        $data = $this->repository->getById($id);
        $return = new EbirthdayData();
        $return->id = $data['id'];
        $return->imgUrl = $data['imgUrl'];
        $return->title = $data['title'];
        $return->isActive = $data['isActive'];
        return $return;
    }
    public function createEbirthday(array $data): int
    {
        // Input validation
        $this->validateNewEbirthday($data);
        $insert_id = $this->repository->insert($data);
        return $insert_id;
    }
    public function updateEbirthday(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "imgUrl"=>$olddata->imgUrl,
            "title"=>$olddata->title,
            "isActive"=>$olddata->isActive
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        $this->validateNewEbirthday($oldarray);
        $this->repository->update($id,$oldarray);
    }
    
    public function get_data(){
        $data = $this->repository->get_data();
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    

    private function validateNewEbirthday(array $data): void
    {
        $errors = [];

        if (empty($data['imgUrl'])) {
            $errors['imgUrl'] = 'Input required';
        }

        if (empty($data['title'])) {
            $errors['title'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }
}