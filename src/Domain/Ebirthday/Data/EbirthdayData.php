<?php
namespace App\Domain\Ebirthday\Data;
final class EbirthdayData
{
    public $id;
    public $imgUrl;
    public $isActive;
    public $title;
}