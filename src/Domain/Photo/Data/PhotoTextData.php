<?php
namespace App\Domain\Photo\Data;
final class PhotoTextData
{
    public $id;
    public $photo_id;
    public $name;
    public $price;
    public $lg;
}