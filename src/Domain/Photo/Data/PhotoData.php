<?php
namespace App\Domain\Photo\Data;
final class PhotoData
{
    public $id;
    public $thumUrl;
    public $imgUrl;
    public $link;
    public $isActive;
    public $texts;
}