<?php
namespace App\Domain\Photo\Repository;
use PDO;

class PhotoRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'thumUrl' => ($data['thumUrl']) ? $data['thumUrl'] : "",
            'imgUrl' => ($data['imgUrl']) ? $data['imgUrl'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO photo SET 
                thumUrl=:thumUrl, 
                imgUrl=:imgUrl, 
                link=:link, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'thumUrl' => ($data['thumUrl']) ? $data['thumUrl'] : "",
            'imgUrl' => ($data['imgUrl']) ? $data['imgUrl'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE photo SET 
                thumUrl=:thumUrl, 
                imgUrl=:imgUrl, 
                link=:link, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function get_total_count($search) : int 
    {
        $datasearch = $search;
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $search = $datasearch;

        $sql = "select count(*) as num from photo where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $datasearch = $param['search'];
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
            $where = "where a.datasearch like '%{$search}%'";
        }
        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,b.name,b.price,a.thumUrl,a.imgUrl,a.link,a.createdtime,a.updatedtime,a.isActive FROM `photo` a left JOIN photo_txt b on a.id = b.photo_id and b.lg = '{$lg}' {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,thumUrl,imgUrl,link,isActive from photo where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Photo not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `photo` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /** method for front-end */
    public function get_active_photos($param){
        $limit = "";
        if(isset($param['limit']) && isset($param['offset'])){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }
        $sql = "select id,thumUrl,imgUrl,link from photo where isActive = 1 order by updatedtime desc {$limit}";

        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function get_active_count(){
        $sql = "select count(*) as num from photo where isActive = 1";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
}
