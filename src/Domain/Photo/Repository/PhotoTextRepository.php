<?php
namespace App\Domain\Photo\Repository;
use PDO;

class PhotoTextRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'photo_id' => $data['photo_id'],
            'name' => ($data['name']) ? $data['name'] : "",
            'price' => ($data['price']) ? $data['price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        $sql = "INSERT INTO photo_txt SET 
                photo_id=:photo_id, 
                name=:name, 
                price=:price, 
                lg=:lg";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'photo_id' => $data['photo_id'],
            'name' => ($data['name']) ? $data['name'] : "",
            'price' => ($data['price']) ? $data['price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "UPDATE photo_txt SET 
                photo_id=:photo_id, 
                name=:name, 
                price=:price, 
                lg=:lg WHERE id =:id";
        $this->connection->prepare($sql)->execute($row);
    }
    public function getByPhotoId(int $id) {
        $sql = "select id,photo_id,name,price,lg from photo_txt where photo_id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        if (!$rows) {
            $rows = [];
        }
        return $rows;
    }
    public function deleteByPhotoId(int $id){
        $sql = "DELETE FROM `photo_txt` WHERE `photo_id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
