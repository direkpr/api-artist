<?php

namespace App\Domain\Photo\Service;
use App\Factory\LoggerFactory;
use App\Domain\Photo\Data\PhotoData;
use App\Domain\Photo\Repository\PhotoRepository;
use App\Domain\Photo\Repository\PhotoTextRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class PhotoService
{

    private $repository;
    private $textRepository;
    private $langsRepository;
    private $logger;

    public function __construct(PhotoRepository $repository,PhotoTextRepository $textRepository,LangsRepository $langsRepository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->textRepository = $textRepository;
        $this->langsRepository = $langsRepository;

        $this->logger = $logger
            ->addFileHandler('PhotoService.log')
            ->createInstance('PhotoService');
    }
    public function deletePhoto(int $id){
        $this->repository->delete($id);
        $this->textRepository->deleteByGoodId($id);
    }
    public function getById(int $id) : PhotoData {
        $data = $this->repository->getById($id);
        $return = new PhotoData();
        $return->id = $data['id'];
        $return->thumUrl = $data['thumUrl'];
        $return->imgUrl = $data['imgUrl'];
        $return->link = $data['link'];
        $return->isActive = $data['isActive'];
        $return->texts = $this->textRepository->getByPhotoId((int)$data['id']);
        return $return;
    }
    public function createPhoto(array $data): int
    {
        // Input validation
        $this->validateNewPhoto($data);
        $datasearch = '';
        foreach($data['texts'] as $text){
            $datasearch .= $text['name'].$text['price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            foreach($data['texts'] as $text){
                $text['photo_id'] = $insert_id;
                if($this->validateTextPhoto($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
        return $insert_id;
    }
    public function updatePhoto(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "thumUrl"=>$olddata->thumUrl,
            "imgUrl"=>$olddata->imgUrl,
            "link"=>$olddata->link,
            "isActive"=>$olddata->isActive,
            "texts"=>$olddata->texts
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        //$this->logger->info(print_r($oldarray,true));
        $this->validateNewPhoto($oldarray);
        $datasearch = '';
        foreach($oldarray['texts'] as $text){
            $datasearch .= $text['name'].$text['price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $oldarray['datasearch'] = $datasearch;
        $this->repository->update($id,$oldarray);
        foreach($oldarray['texts'] as $text){
            if(isset($text['id'])){
                //update
                $text['photo_id'] = $id;
                if($this->validateTextPhoto($text)){
                    $this->textRepository->update($text['id'],$text);
                }
            }else{
                $text['photo_id'] = $id;
                if($this->validateTextPhoto($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $lang = $this->langsRepository->get_default_lang();
        $params['lg'] = $lang->code;
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    private function validateTextPhoto(array $data): bool {
        if(empty($data['lg'])){
            return false;
        }
        if(empty($data['name'])){
            return false;
        }
        if(empty($data['price'])){
            return false;
        }
        return true;
    }

    private function validateNewPhoto(array $data): void
    {
        $errors = [];

        if (empty($data['thumUrl'])) {
            $errors['thumUrl'] = 'Input required';
        }

        if (empty($data['imgUrl'])) {
            $errors['imgUrl'] = 'Input required';
        }

        if (empty($data['link'])) {
            $errors['link'] = 'Input required';
        }

        if(empty($data['texts'])){
            $errors['texts'] = 'Input required';
        }

        $lang = $this->langsRepository->get_default_lang();
        if(!$lang){
            $errors['lang'] = "default lang not found";
            throw new ValidationException('default lang not found', $errors);
        }
        $default_text = null;
        foreach($data['texts'] as $text){
            if($lang['code'] == $text['lg']){
                $default_text = $text;
            }
        }
        if(is_null($default_text)){
            $errors['texts'] = 'default text not found';
            throw new ValidationException('default text not found', $errors);
        }
        if(empty($default_text['name'])){
            $errors['text->name'] = 'Input required';
        }
        if(empty($default_text['price'])){
            $errors['text->price'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    /** All method for web */
    public function get_active_count($params) : int {
        return $this->repository->get_active_count($params);
    }
    public function get_active_photos($params){
        $data = $this->repository->get_active_photos($params);
        if(empty($data) || is_null($data))
            $data = [];
        foreach($data as $key => $val){
            $data[$key]['texts'] = $this->textRepository->getByPhotoId($val['id']);
        }
        return $data;
    }
}