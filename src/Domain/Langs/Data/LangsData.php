<?php
namespace App\Domain\Langs\Data;
final class LangsData
{
    public $id;
    public $code;
    public $title;
    public $isActive;
    public $isDefault;
}