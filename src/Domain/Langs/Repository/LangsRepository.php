<?php
namespace App\Domain\Langs\Repository;
use PDO;

class LangsRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function all_langs($isActive = false){

        $sql = "SELECT id,code,title,isDefault,isActive FROM langs";
        if($isActive){
            $sql .= " where isActive = 1";
        }
        $statement = $this->connection->prepare($sql." order by isDefault desc");
        $statement->execute();
        $row = $statement->fetchAll();
        return $row;
    }
    public function get_default_lang(){
        $sql = "SELECT id,code,title,isDefault,isActive FROM langs WHERE isDefault = 1";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return $row;
    }
}
