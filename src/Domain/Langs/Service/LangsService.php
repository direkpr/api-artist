<?php

namespace App\Domain\Langs\Service;

use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;
use App\Domain\Langs\Data\LangsData;
/**
 * Service.
 */

final class LangsService
{
    private $repository;
    public function __construct(LangsRepository $repository)
    {
        $this->repository = $repository;
    }
    public function getAllLangs($isActive = false){
        $langs = $this->repository->all_langs($isActive);
        return $langs;
    }

}