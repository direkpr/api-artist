<?php

namespace App\Domain\Goods\Service;
use App\Factory\LoggerFactory;
use App\Domain\Goods\Data\GoodsData;
use App\Domain\Goods\Repository\GoodsRepository;
use App\Domain\Goods\Repository\GoodsTextRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class GoodsService
{

    private $repository;
    private $textRepository;
    private $langsRepository;
    private $logger;

    public function __construct(GoodsRepository $repository,GoodsTextRepository $textRepository,LangsRepository $langsRepository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->textRepository = $textRepository;
        $this->langsRepository = $langsRepository;

        $this->logger = $logger
            ->addFileHandler('GoodsService.log')
            ->createInstance('GoodsService');
    }
    public function deleteGoods(int $id){
        $this->repository->delete($id);
        $this->textRepository->deleteByGoodId($id);
    }
    public function getById(int $id) : GoodsData {
        $data = $this->repository->getById($id);
        $return = new GoodsData();
        $return->id = $data['id'];
        $return->imgUrl = $data['imgUrl'];
        $return->link = $data['link'];
        $return->isActive = $data['isActive'];
        $return->texts = $this->textRepository->getByGoodsId((int)$data['id']);
        return $return;
    }
    public function createGoods(array $data): int
    {
        // Input validation
        $this->validateNewGoods($data);
        $datasearch = '';
        foreach($data['texts'] as $text){
            $datasearch .= $text['good_name'].$text['good_type'].$text['good_code'].$text['good_price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            foreach($data['texts'] as $text){
                $text['good_id'] = $insert_id;
                if($this->validateTextGoods($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
        return $insert_id;
    }
    public function updateGoods(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "imgUrl"=>$olddata->imgUrl,
            "link"=>$olddata->link,
            "isActive"=>$olddata->isActive,
            "texts"=>$olddata->texts
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        //$this->logger->info(print_r($oldarray,true));
        $this->validateNewGoods($oldarray);
        $datasearch = '';
        foreach($oldarray['texts'] as $text){
            $datasearch .= $text['good_name'].$text['good_type'].$text['good_code'].$text['good_price'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $oldarray['datasearch'] = $datasearch;
        $this->repository->update($id,$oldarray);
        foreach($oldarray['texts'] as $text){
            if(isset($text['id'])){
                //update
                $text['good_id'] = $id;
                if($this->validateTextGoods($text)){
                    $this->textRepository->update($text['id'],$text);
                }
            }else{
                $text['good_id'] = $id;
                if($this->validateTextGoods($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $lang = $this->langsRepository->get_default_lang();
        $params['lg'] = $lang->code;
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    private function validateTextGoods(array $data): bool {
        if(empty($data['lg'])){
            return false;
        }
        if(empty($data['good_name'])){
            return false;
        }
        if(empty($data['good_type'])){
            return false;
        }
        if(empty($data['good_code'])){
            return false;
        }
        if(empty($data['good_price'])){
            return false;
        }
        return true;
    }

    private function validateNewGoods(array $data): void
    {
        $errors = [];

        if (empty($data['imgUrl'])) {
            $errors['imgUrl'] = 'Input required';
        }

        if (empty($data['link'])) {
            $errors['link'] = 'Input required';
        }

        if(empty($data['texts'])){
            $errors['texts'] = 'Input required';
        }

        $lang = $this->langsRepository->get_default_lang();
        if(!$lang){
            $errors['lang'] = "default lang not found";
            throw new ValidationException('default lang not found', $errors);
        }
        //var_dump($lang['code']);
        $default_text = null;
        foreach($data['texts'] as $text){
            if($lang['code'] == $text['lg']){
                $default_text = $text;
            }
        }
        //var_dump($default_text);
        if(is_null($default_text)){
            $errors['texts'] = 'default text not found';
            throw new ValidationException('default text not found', $errors);
        }
        if(empty($default_text['good_name'])){
            $errors['text->good_name'] = 'Input required';
        }
        if(empty($default_text['good_type'])){
            $errors['text->good_type'] = 'Input required';
        }
        if(empty($default_text['good_code'])){
            $errors['text->good_code'] = 'Input required';
        }
        if(empty($default_text['good_price'])){
            $errors['text->good_price'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }

    /** All method for web */
    public function get_active_count($params) : int {
        return $this->repository->get_active_count($params);
    }
    public function get_active_goods($params){
        $data = $this->repository->get_active_goods($params);
        if(empty($data) || is_null($data))
            $data = [];
        foreach($data as $key => $val){
            $data[$key]['texts'] = $this->textRepository->getByGoodsId($val['id']);
        }
        return $data;
    }
}