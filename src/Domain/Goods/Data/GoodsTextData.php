<?php
namespace App\Domain\Goods\Data;
final class GoodsTextData
{
    public $id;
    public $good_id;
    public $name;
    public $type;
    public $code;
    public $price;
    public $lg;
}