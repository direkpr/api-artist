<?php
namespace App\Domain\Goods\Data;
final class GoodsData
{
    public $id;
    public $imgUrl;
    public $link;
    public $isActive;
    public $texts;
}