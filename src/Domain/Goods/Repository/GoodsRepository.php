<?php
namespace App\Domain\Goods\Repository;
use PDO;

class GoodsRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'imgUrl' => ($data['imgUrl']) ? $data['imgUrl'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO goods SET 
                imgUrl=:imgUrl, 
                link=:link, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'imgUrl' => ($data['imgUrl']) ? $data['imgUrl'] : "",
            'link' => ($data['link']) ? $data['link'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE goods SET 
                imgUrl=:imgUrl, 
                link=:link, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function get_total_count($search) : int 
    {
        $datasearch = $search;
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
        $sql = "select count(*) as num from goods where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $datasearch = $param['search'];
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
            $where = "where a.datasearch like '%{$search}%'";
        }
        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,b.good_name,b.good_type,b.good_code,b.good_price,a.imgUrl,a.link,a.createdtime,a.updatedtime,a.isActive FROM `goods` a left JOIN goods_txt b on a.id = b.good_id and b.lg = '{$lg}' {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,imgUrl,link,isActive from goods where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Goods not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `goods` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /** method for front-end */
    public function get_active_goods($param){
        $limit = "";
        if(isset($param['limit']) && isset($param['offset'])){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }
        $sql = "select id,imgUrl,link from goods where isActive = 1 order by updatedtime desc {$limit}";

        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function get_active_count(){
        $sql = "select count(*) as num from goods where isActive = 1";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
}
