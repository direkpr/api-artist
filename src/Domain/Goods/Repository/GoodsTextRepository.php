<?php
namespace App\Domain\Goods\Repository;
use PDO;

class GoodsTextRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'good_id' => $data['good_id'],
            'good_name' => ($data['good_name']) ? $data['good_name'] : "",
            'good_type' => ($data['good_type']) ? $data['good_type'] : "",
            'good_code' => ($data['good_code']) ? $data['good_code'] : "",
            'good_price' => ($data['good_price']) ? $data['good_price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "INSERT INTO goods_txt SET 
                good_id=:good_id, 
                good_name=:good_name, 
                good_type=:good_type, 
                good_code=:good_code, 
                good_price=:good_price, 
                lg=:lg";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'good_id' => $data['good_id'],
            'good_name' => ($data['good_name']) ? $data['good_name'] : "",
            'good_type' => ($data['good_type']) ? $data['good_type'] : "",
            'good_code' => ($data['good_code']) ? $data['good_code'] : "",
            'good_price' => ($data['good_price']) ? $data['good_price'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "UPDATE goods_txt SET 
                good_id=:good_id, 
                good_name=:good_name, 
                good_type=:good_type, 
                good_code=:good_code, 
                good_price=:good_price, 
                lg=:lg WHERE id =:id";
        $this->connection->prepare($sql)->execute($row);
    }
    public function getByGoodsId(int $id) {
        $sql = "select id,good_id,good_name,good_type,good_code,good_price,lg from goods_txt where good_id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        if (!$rows) {
            $rows = [];
        }
        return $rows;
    }
    public function deleteByGoodId(int $id){
        $sql = "DELETE FROM `goods_txt` WHERE `good_id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
