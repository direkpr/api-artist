<?php

namespace App\Domain\Omise\Repository;

use PDO;

/**
 * Repository.
 */
class CustomerRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function get_today_monthlysubscription(){
        $date = date("Y-m-d");
        $sql = "SELECT * FROM `customer` where subscription_type = 2 and DATE_FORMAT(next_billdate,'%Y-%m-%d') = '".$date."'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function check_today_recurring_purchases($member_id){
        $date = date("Y-m-d");
        $sql = "SELECT * FROM `purchases` WHERE member_id = {$member_id} and createdtime LIKE '{$date}%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return $row;
    }

    public function updateExpiredDate($omise_schedule_id,$expire_date){
        //$next_billdate = new DateTime($expire_date);
        $row2 = [
            'omise_schedule_id'=>$omise_schedule_id,
            'expired' => $expire_date,
            'next_billdate'=>$expire_date,
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql2 = "UPDATE customer SET 
                expired=:expired, 
                next_billdate=:next_billdate, 
                updatedtime=:updatedtime
                WHERE omise_schedule_id=:omise_schedule_id";
        
        return $this->connection->prepare($sql2)->execute($row2);
    }

    public function insertOrUpdateCustomer(array $customer) {
        $cust = $this->getCustomerByMemberId((int)$customer['member_id']);
        if($cust){
            $this->updateCustomer($customer);
            return $cust->id;
        }else{
            return $this->insertCustomer($customer);
        }
        return -1;
    }

    public function insertCustomer(array $customer): int
    {
        $row = [
            'member_id' => ($customer['member_id']) ? $customer['member_id'] : -1,
            'omise_cust_id' => ($customer['omise_cust_id']) ? $customer['omise_cust_id'] : "",
            'omise_schedule_id' => ($customer['omise_schedule_id']) ? $customer['omise_schedule_id'] : "",
            'subscription_type' => ($customer['subscription_type']) ? $customer['subscription_type'] : 0,
            'expired' => ($customer['expired']) ? $customer['expired'] : null,
            'next_billdate' => ($customer['next_billdate']) ? $customer['next_billdate'] : null,
            'createdtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO customer SET 
                member_id=:member_id, 
                omise_cust_id=:omise_cust_id, 
                omise_schedule_id=:omise_schedule_id,
                subscription_type=:subscription_type, 
                expired=:expired, 
                next_billdate=:next_billdate, 
                createdtime=:createdtime;";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();

    }
    public function updateMemberPaymentStatus($id,$payment_status){
        $sql = "UPDATE member SET payment_status=:payment_status,roles=:roles WHERE id=:id";
        $this->connection->prepare($sql)->execute(array("id"=>$id,"payment_status"=>$payment_status,"roles"=>json_encode(["customer"])));
    }
    public function updateCustomer(array $customer){
        $olddata = $this->getCustomerByMemberId($customer['member_id']);
        if(!$olddata){
            return false;
        }

        $row = [
            'member_id' => ($customer['member_id']) ? $customer['member_id'] :$olddata['member_id'],
            'omise_cust_id' => ($customer['omise_cust_id']) ? $customer['omise_cust_id'] : $olddata['omise_cust_id'],
            'subscription_type' => ($customer['subscription_type']) ? $customer['subscription_type'] : $olddata['subscription_type'],
            'omise_schedule_id' => ($customer['omise_schedule_id']) ? $customer['omise_schedule_id'] : $olddata['omise_schedule_id'],
            'expired' => ($customer['expired']) ? $customer['expired'] : $olddata['expired'],
            'next_billdate' => ($customer['next_billdate']) ? $customer['next_billdate'] : $olddata['next_billdate'],
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE customer SET 
                omise_cust_id=:omise_cust_id, 
                omise_schedule_id=:omise_schedule_id,
                subscription_type=:subscription_type, 
                expired=:expired, 
                next_billdate=:next_billdate, 
                updatedtime=:updatedtime
                WHERE member_id=:member_id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function memberExisting($member_id):bool{
        $sql = "SELECT count(*) as num FROM customer WHERE member_id = :member_id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['member_id' => $member_id]);
        $row = $statement->fetch();
        if((int)$row['num'] > 0){
            return true;
        }else{
            return false;
        }
    }
    public function getCustomerById($id){
        $sql = "SELECT * FROM customer WHERE id = :id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['id' => $id]);
        $row = $statement->fetch();
        if(!$row){
            return null;
        }else{
            return $row;
        }
    }
    public function getCustomerByMemberId($member_id){
        $sql = "SELECT * FROM customer WHERE member_id = :member_id;";
        $statement = $this->connection->prepare($sql);
        $statement->execute(['member_id' => $member_id]);
        $row = $statement->fetch();
        if(!$row){
            return null;
        }else{
            return $row;
        }
    }
    public function getLastInvoiceId():int {
        $sql = "SELECT max(id) as last_id FROM `purchases`";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if(!$row){
            return 1;
        }else{
            return ((int)$row['last_id'] + 1);
        }
    }
    public function insertPurchase(array $data){
        $row = [
            'member_id' => ($data['member_id']) ? $data['member_id'] : -1,
            'invoice' => ($data['invoice']) ? $data['invoice'] : "",
            'amount' => ($data['amount']) ? $data['amount'] : 0,
            'currency' => ($data['currency']) ? $data['currency'] : "JPY",
            'status' => 0,
            'remark' => ($data['remark']) ? $data['remark'] : "",
            'omise_charge_id'=>"",
            'authorize_uri'=>"",
            'subscription_type'=>($data['subscription_type']) ? $data['subscription_type'] : 0,
            'isRenew'=>($data['isRenew']) ? $data['isRenew'] : 0,
            'createdtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO purchases SET 
                member_id=:member_id, 
                invoice=:invoice, 
                amount=:amount, 
                currency=:currency, 
                status=:status, 
                remark=:remark, 
                omise_charge_id=:omise_charge_id,
                authorize_uri=:authorize_uri,
                isRenew=:isRenew,
                subscription_type=:subscription_type,
                createdtime=:createdtime;";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function getPurchaseByInvoice($invoice){
        $sql = "SELECT * FROM `purchases` wHERE invoice = :invoice";
        $statement = $this->connection->prepare($sql);
        $statement->execute(array("invoice"=>$invoice));
        $row = $statement->fetch();
        if(!$row){
            return false;
        }else{
            return $row;
        }
    }
    public function getPurchaseByInvoiceId($id){
        $sql = "SELECT * FROM `purchases` wHERE id = :id";
        $statement = $this->connection->prepare($sql);
        $statement->execute(array("id"=>$id));
        $row = $statement->fetch();
        if(!$row){
            return false;
        }else{
            return $row;
        }
    }
    public function updatePurchaseByInvoice(array $data){
        $olddata = $this->getPurchaseByInvoice($data['invoice']);
        if(!$olddata){
            return false;
        }

        $row = [
            'member_id' => ($data['member_id']) ? $data['member_id'] : $olddata['member_id'],
            'invoice' => ($data['invoice']) ? $data['invoice'] : $olddata['invoice'],
            'amount' => ($data['amount']) ? $data['amount'] : $olddata['amount'],
            'currency' => ($data['currency']) ? $data['currency'] : $olddata['currency'],
            'status' => ($data['status']) ? $data['status'] : $olddata['status'],
            'remark' => ($data['remark']) ? $data['remark'] : $olddata['remark'],
            'authorize_uri' => ($data['authorize_uri']) ? $data['authorize_uri'] : $olddata['authorize_uri'],
            'omise_charge_id' => ($data['omise_charge_id']) ? $data['omise_charge_id'] : $olddata['omise_charge_id'],
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "UPDATE purchases SET 
                member_id=:member_id, 
                invoice=:invoice, 
                amount=:amount, 
                currency=:currency, 
                status=:status, 
                remark=:remark,
                omise_charge_id=:omise_charge_id,
                authorize_uri=:authorize_uri,
                updatedtime=:updatedtime
                WHERE invoice=:invoice;";

        $this->connection->prepare($sql)->execute($row);
    }
}
