<?php

namespace App\Domain\Omise\Service;
use Slim\App;
use App\Factory\LoggerFactory;
use App\Domain\Omise\Repository\CustomerRepository;
use App\Domain\Member\Repository\MemberReaderRepository;
use App\Exception\ValidationException;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
use DomainException;
use OmiseCustomer;
use OmiseCharge;
use OmiseSchedule;
use DateTime;
/**
 * Service.
 */
final class Customer
{
    /**
     * @var MemberCreatorRepository
     */
    private $customerRepository;
    private $logger;
    public $error = null;
    private $app;
    private $memberRepository;
    public function __construct(CustomerRepository $customerRepository,LoggerFactory $logger,App $app,MemberReaderRepository $memberRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->memberRepository = $memberRepository;
        $this->app = $app;
        $this->logger = $logger
            ->addFileHandler('customer.log')
            ->createInstance('customer-service');
    }

    public function updateRecurring(){
        //update recurring 
        $today_member_expired = $this->customerRepository->get_today_monthlysubscription();
        if($today_member_expired && count($today_member_expired) > 0){
            foreach($today_member_expired as $customer){
                if(isset($customer['omise_cust_id']) && isset($customer['omise_schedule_id'])){
                    $omise_schedule_id = $customer['omise_schedule_id'];
                    $schedule = OmiseSchedule::retrieve($omise_schedule_id);
                    //$occurrences = $schedule->occurrences();
                    //$this->logger->info(print_r($schedule,true));
                    $status = $schedule->offsetGet('status');
                    $next_occurrences_on = $schedule->offsetGet("next_occurrences_on");
                    switch($status){
                        case "running" : 
                        case "expiring" : 
                            $todayPurchase = $this->customerRepository->check_today_recurring_purchases($customer['member_id']);
                            if(!isset($todayPurchase->id)){
                                $occurrences = $schedule->occurrences(array("order"=>"reverse_chronological"));
                                //var_dump($occurrences['data'][0]);
                                $occurrence = $occurrences['data'][0];
                                $billdate = new DateTime($customer['next_billdate']);
                                if($occurrence['scheduled_on'] == $billdate->format("Y-m-d") && $occurrence['status'] == "successful"){
                                    // update expired date
                                    if(isset($next_occurrences_on[0])){
                                        $this->customerRepository->updateExpiredDate($omise_schedule_id,$next_occurrences_on[0]);
                                        $this->logger->info($omise_schedule_id."::update next schedule =>".$next_occurrences_on[0]);
                                    }
                                    //no recording in purchases
                                    $this->logger->info(print_r(array("member_id"=>$customer['member_id']),true));
                                    $invoice = $this->createInvoice($customer['member_id'],MONTHLY_PURCHASE_AMT,CURRENCY_PURCHASE,1,2);
                                    $this->customerRepository->updatePurchaseByInvoice(array(
                                        "invoice"=>$invoice['invoice'],
                                        "status"=>1
                                    ));
                                    $this->sendmailThank($invoice['invoice'],true);
                                }else if($occurrence['scheduled_on'] == $billdate->format("Y-m-d") && $occurrence['status'] == "failed"){
                                    $schedule = OmiseSchedule::retrieve($omise_schedule_id);
                                    $schedule->destroy();
                                }
                            }
                            break;
                        case 'deleted' :
                        case 'suspended' : 
                        case 'expired' :
                            //$this->customerRepository->updateExpiredDate($omise_schedule_id,date('Y-m-d'));
                            //$this->logger->info($omise_schedule_id."::update next schedule =>".date('Y-m-d'));
                            break;
                        default :
                        //not thing heere 
                        break;
                    }
                }
            }
        }
        return true;
    }

    public function unsubscription($params){
        $customer = $this->customerRepository->getCustomerByMemberId($params['memberId']);
        //$this->logger->info(print_r($customer,true));
        if($customer && isset($customer['omise_schedule_id'])){
            $schedule = OmiseSchedule::retrieve($customer['omise_schedule_id']);
            $schedule->destroy();
            $this->logger->info(print_r($schedule,true));
            $customer['subscription_type'] = 3;
            $this->customerRepository->updateCustomer($customer);
            return true;
        }else{
            return false;
        }
    }
    public function updateSchedulCustomer($omise_schedule_id){
        $schedule = OmiseSchedule::retrieve($omise_schedule_id);
        $this->logger->info(print_r($schedule,true));
        $status = $schedule->offsetGet('status');
        $next_occurrences_on = $schedule->offsetGet("next_occurrences_on");
        switch($status){
            case "running" : 
            case "expiring" : 
                // update expired date
                /*if(isset($next_occurrences_on[0])){
                    $this->customerRepository->updateExpiredDate($omise_schedule_id,$next_occurrences_on[0]);
                }*/
                break;
            case 'deleted' :
            case 'suspended' : 
            case 'expired' :
                //$this->customerRepository->updateExpiredDate($omise_schedule_id,date('Y-m-d'));
                //$this->logger->info($omise_schedule_id."::update next schedule =>".date('Y-m-d'));
            break;
            default :
            //not thing heere 
            break;
        }
    }   

    public function invoice($invoice){
        return $this->customerRepository->getPurchaseByInvoice($invoice);
    }
    public function invoiceById($invoiceId){
        return $this->customerRepository->getPurchaseByInvoiceId($invoiceId);
    }
    public function update_invoice($invoice){
        $data = $this->customerRepository->getPurchaseByInvoice($invoice);
        if($data['status'] == 0) {
            $charge = OmiseCharge::retrieve($data["omise_charge_id"]);
            if($charge->offsetGet('object') == "charge"){
                $status = $charge->offsetGet('status');
                switch($status){
                    case "successful" :
                        $date = new DateTime();
                        $date->modify('+1 month');
                        $nextdatetime = $date->format('Y-m-d H:i:s');
                        $this->customerRepository->updateCustomer(array(
                            "member_id"=>$data['member_id'],
                            'subscription_type'=>$data['subscription_type'],
                            "expired"=>$nextdatetime,
                            "next_billdate"=>$nextdatetime
                        ));
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice,
                            "status"=>1
                        ));
                        $this->customerRepository->updateMemberPaymentStatus($data['member_id'],1);
                        if($data['isRenew'] == 1){
                            $this->sendmailThank($invoice,true);
                        }else{
                            $this->sendmailThank($invoice);
                            $this->sendmailAdmin($invoice);
                        }
                        
                    break;
                    case "failed":
                        $failure_message = $charge->offsetGet('failure_message');
                        $this->logger->error($failure_message);
                        $this->error = $failure_message;
                        $return_status = false;
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice,
                            "status"=>2,
                            "remark"=>$failure_message
                        ));
                    break;
                    case "pending" : 
                    break;
                }
                return $this->customerRepository->getPurchaseByInvoice($invoice);
            }else{
                return $data;
            }
        }else{
            return $data;
        }
        
    }
    public function createCustomer(array $data)
    {
        if(!$this->customerRepository->memberExisting($data['membe_id'])){
            //new member

            //1. create omise customer
            $customer = OmiseCustomer::create(array(
                'email' => $data['email'],
                'description' => $data['firstname']." ".$data['lastname']." (id : ".$data['member_id'].") "
            ));
            //var_dump($customer);
            //$this->logger->info(print_r($customer->getValue(),true));
            $omise_cust_id = $customer->offsetGet('id');
            //$this->logger->info($omise_cust_id);
            if($omise_cust_id){
                $cust_data = array(
                    "member_id"=>$data['member_id'],
                    "omise_cust_id"=>$omise_cust_id,
                    "subscription_type"=>$data['subscription_type'],
                    "expired"=>null,
                    "next_billdate"=>null
                );
                $realme_customer_id = $this->customerRepository->insertOrUpdateCustomer($cust_data);
                if($realme_customer_id > 0){
                    return $this->customerRepository->getCustomerById($realme_customer_id);
                }else{
                    $this->logger->error("save customer data fail!");
                    return false;
                }
            }else{
                $this->logger->error("created omise customer fail!");
                return false;
            }

        }else{
            return $this->customerRepository->getCustomerByMemberId($data['member_id']);
        }
    }
    public function createInvoice($member_id,$amount,$currency = "JPY",$isRenew = 0,$subscription_type = 0){
        $formatted_value = sprintf("%05d", $this->customerRepository->getLastInvoiceId());
        $invoice = date('Ymd').$formatted_value;
        $invoiceId =  $this->customerRepository->insertPurchase(array(
            "member_id"=>$member_id,
            "invoice"=>$invoice,
            "amount"=>$amount,
            "currency"=>$customer,
            "status"=>0,
            "isRenew"=>$isRenew,
            "remark"=>"Purchase subscription memberId #".$member_id,
            "subscription_type"=> $subscription_type
        ));
        return $this->customerRepository->getPurchaseByInvoice($invoice);
    }
    public function schedulecharge(array $params){
        if(!isset($params['invoiceId'])){
            $this->logger->error("charge fail::invoiceId not found");
            $this->error = "charge fail::invoiceId not found";
            return false;
        }

        if(!isset($params['card_token'])){
            $this->logger->error("charge fail::card_token not found");
            $this->error = "charge fail::card_token not found";
            return false;
        }

        $invoice = $this->customerRepository->getPurchaseByInvoiceId((int)$params['invoiceId']);
        if(!$invoice){
            $this->logger->error("charge fail::invoice data not found");
            $this->error = "charge fail::invoice data not found";
            return false;
        }

        $customer = $this->customerRepository->getCustomerByMemberId((int)$invoice['member_id']);
        if(!$customer){
            $this->logger->error("charge fail::customer data not found");
            $this->error = "charge fail::customer data not found";
            return false;
        }

        $omise_customer = OmiseCustomer::retrieve($customer['omise_cust_id']);
        $this->logger->info(print_r($omise_customer,true));
        $omise_cust_id = $omise_customer->offsetGet('id');
        if(!$omise_customer){
            $this->logger->error("charge fail::omise customer data not found");
            $this->error = "charge fail::omise customer data not found";
            return false;
        }
        $response = $omise_customer->update(array(
            'card' => $params['card_token']
        ));
        $this->logger->info(print_r($response,true));
        if($response->offsetGet('object') == "card"){
            $card_id = $response->offsetGet('id');
            $this->logger->info($card_id);
            $defaultcard = $omise_customer->update(array(
                'default_card' => $card_id
            ));
            $this->logger->info(print_r($defaultcard,true));
        }

        $charge = OmiseCharge::create(array(
            'amount' => $invoice['amount'],
            'currency' => $invoice['currency'],
            'customer' => $omise_cust_id,
            'return_uri'=>RETURN_URI.$invoice['invoice']."/complete",
            'metadata'=>array("invoice"=>$invoice['invoice'])
        ));
        $this->logger->info(print_r($charge,true));
        if($charge->offsetGet('object') == "charge"){
            $status = $charge->offsetGet('status');
            $return_status = false;
            switch($status){
                case "successful" :
                    $return_status = true;
                    $date = new DateTime();
                    $date->modify('+1 month');
                    $nextdatetime = $date->format('Y-m-d H:i:s');
                    $this->customerRepository->updateCustomer(array(
                        "member_id"=>$customer['member_id'],
                        'subscription_type'=>2,
                        "expired"=>$nextdatetime,
                        "next_billdate"=>$nextdatetime
                    ));
                    $this->customerRepository->updatePurchaseByInvoice(array(
                        "invoice"=>$invoice['invoice'],
                        "status"=>1
                    ));
                    $this->customerRepository->updateMemberPaymentStatus($customer['member_id'],1);


                    //first charge complete , we will be creatd schedule charge here... 
                    $scheduler = OmiseCharge::schedule(array(
                        'amount' => $invoice['amount'],
                        'customer' => $omise_cust_id,
                        'description' => 'Membership fee'
                    ));
                    $nextYear = new Date();
                    $nextYear->modify('+1 year');
                    $schedule = $scheduler->every(1)
                      ->month(array(intval($date->format('d'))))
                      ->startDate($date->format('Y-m-d'))
                      ->endDate($nextYear->format('Y-m-d'))
                      ->start();


                    if($invoice['isRenew'] == 1){
                        $this->sendmailThank($invoice['invoice'],true);
                    }else{
                        $this->sendmailThank($invoice['invoice']);
                        $this->sendmailAdmin($invoice['invoice']);
                    }
                    
                break;
                case "failed":
                    $failure_message = $charge->offsetGet('failure_message');
                    $this->logger->error($failure_message);
                    $this->error = $failure_message;
                    $return_status = false;
                    $this->customerRepository->updatePurchaseByInvoice(array(
                        "invoice"=>$invoice['invoice'],
                        "status"=>0,
                        "remark"=>$failure_message
                    ));
                break;
                case "pending" : 
                    $return_status = true;
                    $omise_charge_id = $charge->offsetGet('id');
                    $authorize_uri = $charge->offsetGet('authorize_uri');
                    $this->customerRepository->updatePurchaseByInvoice(array(
                        "invoice"=>$invoice['invoice'],
                        "status"=>0,
                        "authorize_uri"=>$authorize_uri,
                        "omise_charge_id"=>$omise_charge_id
                    ));
                break;
            }
            return $return_status;
        }else{
            $this->logger->error("charge fail::call api omise charge fail");
            $this->error = "charge fail::call api omise charge fail";
            return false;
        }



    }
    public function charge(array $params){
        if(!isset($params['invoiceId'])){
            $this->logger->error("charge fail::invoiceId not found");
            $this->error = "charge fail::invoiceId not found";
            return false;
        }

        if(!isset($params['card_token'])){
            $this->logger->error("charge fail::card_token not found");
            $this->error = "charge fail::card_token not found";
            return false;
        }

        $invoice = $this->customerRepository->getPurchaseByInvoiceId((int)$params['invoiceId']);
        if(!$invoice){
            $this->logger->error("charge fail::invoice data not found");
            $this->error = "charge fail::invoice data not found";
            return false;
        }

        $customer = $this->customerRepository->getCustomerByMemberId((int)$invoice['member_id']);
        if(!$customer){
            $this->logger->error("charge fail::customer data not found");
            $this->error = "charge fail::customer data not found";
            return false;
        }

        $omise_customer = OmiseCustomer::retrieve($customer['omise_cust_id']);
        $this->logger->info(print_r($omise_customer,true));
        $omise_cust_id = $omise_customer->offsetGet('id');
        if(!$omise_customer){
            $this->logger->error("charge fail::omise customer data not found");
            $this->error = "charge fail::omise customer data not found";
            return false;
        }
        $default_card = $omise_customer->offsetGet('default_card');
        $this->logger->info("before delete ::".$default_card);
        if(!is_null($default_card)){
            $oldcard = $omise_customer->getCards()->retrieve($default_card);
            $oldcard->destroy();
            $oldcard->isDestroyed(); # => true
        }

        //update default card to omise customer
        $response = $omise_customer->update(array(
            'card' => $params['card_token']
        ));
        $omise_customer->reload();
        $default_card = $omise_customer->offsetGet('default_card');
        $this->logger->info("after update ::".$default_card);
        /*
        $charge = OmiseCharge::create(array(
            'amount' => $invoice['amount'],
            'currency' => $invoice['currency'],
            'card' => $params['card_token'],
            'return_uri'=>RETURN_URI.$invoice['invoice']."/complete",
            'metadata'=>array("invoice"=>$invoice['invoice'])
        ));
        */

        //New Version 
        if($invoice['subscription_type'] == 1){
            $charge = OmiseCharge::create(array(
                'amount' => $invoice['amount'],
                'currency' => $invoice['currency'],
                'customer' => $omise_cust_id,
                'return_uri'=>RETURN_URI.$invoice['invoice']."/complete",
                'metadata'=>array("invoice"=>$invoice['invoice'])
            ));
            $this->logger->info(print_r($charge,true));
            if($charge->offsetGet('object') == "charge"){
                $status = $charge->offsetGet('status');
                $return_status = false;
                switch($status){
                    case "successful" :
                        $return_status = true;
                        $date = new DateTime();
                        
                        if(TEST_MODE){
                            $date->modify('+2 day');
                        }else{
                            $date->modify('+1 month');
                        }
                        
                        
                        $nextdatetime = $date->format('Y-m-d H:i:s');
                        $this->customerRepository->updateCustomer(array(
                            "member_id"=>$customer['member_id'],
                            'subscription_type'=>$invoice['subscription_type'],
                            "expired"=>$nextdatetime,
                            "next_billdate"=>$nextdatetime
                        ));
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice['invoice'],
                            "status"=>1
                        ));
                        $this->customerRepository->updateMemberPaymentStatus($customer['member_id'],1);

                        if($invoice['isRenew'] == 1){
                            $this->sendmailThank($invoice['invoice'],true);
                        }else{
                            $this->sendmailThank($invoice['invoice']);
                            $this->sendmailAdmin($invoice['invoice']);
                        }
                        
                    break;
                    case "failed":
                        $failure_message = $charge->offsetGet('failure_message');
                        $this->logger->error($failure_message);
                        $this->error = $failure_message;
                        $return_status = false;
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice['invoice'],
                            "status"=>0,
                            "remark"=>$failure_message
                        ));
                    break;
                    case "pending" : 
                        $return_status = true;
                        $omise_charge_id = $charge->offsetGet('id');
                        $authorize_uri = $charge->offsetGet('authorize_uri');
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice['invoice'],
                            "status"=>0,
                            "authorize_uri"=>$authorize_uri,
                            "omise_charge_id"=>$omise_charge_id
                        ));
                    break;
                }
                return $return_status;
            }else{
                $this->logger->error("charge fail::call api omise charge fail");
                $this->error = "charge fail::call api omise charge fail";
                return false;
            }
        }else if($invoice['subscription_type'] == 2){
            //monthly subscriber
            $date = new DateTime();
            //For test 
            //$date->modify('+2 day');

            //$date->modify('+1 month');

            $scheduler = OmiseCharge::schedule(array(
                'amount' => $invoice['amount'],
                'currency' => $invoice['currency'],
                'customer' => $omise_cust_id,
                'description' => 'Membership fee'
            ));
            $nextYear = new DateTime();
            $nextYear->modify('+1 year');

            //FORTEST
            if(TEST_MODE){
                $schedule = $scheduler->every(2)
                ->days()
                ->startDate($date->format('Y-m-d'))
                ->endDate($nextYear->format('Y-m-d'))
                ->start();
            }else{
                $schedule = $scheduler->every(1)
                ->month(array(intval($date->format('d'))))
                ->startDate($date->format('Y-m-d'))
                ->endDate($nextYear->format('Y-m-d'))
                ->start();
            }

            
            
            

            $this->logger->info(print_r($schedule,true));
            //update omise schedule id to customer
            $omise_schedule_id = $schedule->offsetGet('id');
            $occurrences = $schedule->offsetGet('occurrences');
            $next_occurrences_on = $schedule->offsetGet('next_occurrences_on');
            $this->logger->info($omise_schedule_id);
            $this->logger->info(print_r($occurrences,true));
            //$this->logger->info(print_r($occurrences['data'],true));
            //$this->logger->info(print_r($occurrences['data'][0],true));
            //$this->logger->info(print_r($occurrences['data'][0]['status'],true));
            $this->logger->info(print_r($next_occurrences_on,true));

            $this->customerRepository->updateCustomer(array(
                "member_id"=>$customer['member_id'],
                'omise_schedule_id'=>$omise_schedule_id
            ));

            if(is_array($occurrences) && count($occurrences) > 0){
                $occurrence = $occurrences['data'][0];
                $return_status = false;
                switch($occurrence['status']){
                    case 'successful' : 
                        $return_status = true;
                        $this->customerRepository->updateCustomer(array(
                            "member_id"=>$customer['member_id'],
                            'subscription_type'=>$invoice['subscription_type'],
                            "expired"=>$next_occurrences_on[0],
                            "next_billdate"=>$next_occurrences_on[0]
                        ));
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice['invoice'],
                            "status"=>1
                        ));
                        $this->customerRepository->updateMemberPaymentStatus($customer['member_id'],1);

                        if($invoice['isRenew'] == 1){
                            $this->sendmailThank($invoice['invoice'],true);
                        }else{
                            $this->sendmailThank($invoice['invoice']);
                            $this->sendmailAdmin($invoice['invoice']);
                        }
                        break;
                    case "failed":
                        $schedule = OmiseSchedule::retrieve($omise_schedule_id);
                        $schedule->destroy();
                        $failure_message = $occurrence['message'];
                        $this->logger->error($failure_message);
                        $this->error = $failure_message;
                        $return_status = false;
                        $this->customerRepository->updatePurchaseByInvoice(array(
                            "invoice"=>$invoice['invoice'],
                            "status"=>0,
                            "remark"=>$failure_message
                        ));
                        break;
                    case "pending" : 
                        break;
                }
                return $return_status;
            }else{
                //charge not complete , we will delete this schedule
                $schedule = OmiseSchedule::retrieve($omise_schedule_id);
                $schedule->destroy();
                $this->logger->error("charge fail::call api omise charge fail");
                $this->error = "charge fail::call api omise charge fail";
                return false;
            }

        }else{
            $this->logger->error("charge fail::unknow subscription type");
            $this->error = "charge fail::unknow subscription type";
            return false;
        }


        //Old Version
        /*
        $charge = OmiseCharge::create(array(
            'amount' => $invoice['amount'],
            'currency' => $invoice['currency'],
            'customer' => $omise_cust_id,
            'return_uri'=>RETURN_URI.$invoice['invoice']."/complete",
            'metadata'=>array("invoice"=>$invoice['invoice'])
        ));
        $this->logger->info(print_r($charge,true));
        if($charge->offsetGet('object') == "charge"){
            $status = $charge->offsetGet('status');
            $return_status = false;
            switch($status){
                case "successful" :
                    $return_status = true;
                    $date = new DateTime();
                    
                    //For test 
                    $date->modify('+2 day');

                    //$date->modify('+1 month');
                    
                    
                    $nextdatetime = $date->format('Y-m-d H:i:s');
                    $this->customerRepository->updateCustomer(array(
                        "member_id"=>$customer['member_id'],
                        'subscription_type'=>$invoice['subscription_type'],
                        "expired"=>$nextdatetime,
                        "next_billdate"=>$nextdatetime
                    ));
                    $this->customerRepository->updatePurchaseByInvoice(array(
                        "invoice"=>$invoice['invoice'],
                        "status"=>1
                    ));
                    $this->customerRepository->updateMemberPaymentStatus($customer['member_id'],1);


                    if($invoice['subscription_type'] == 2){
                        //first charge complete , we will be creatd schedule charge here... 
                        $scheduler = OmiseCharge::schedule(array(
                            'amount' => $invoice['amount'],
                            'currency' => $invoice['currency'],
                            'customer' => $omise_cust_id,
                            'description' => 'Membership fee'
                        ));
                        $nextYear = new DateTime();
                        $nextYear->modify('+1 year');

                        //FORTEST
                        $schedule = $scheduler->every(2)
                        ->days()
                        ->startDate($date->format('Y-m-d'))
                        ->endDate($nextYear->format('Y-m-d'))
                        ->start();

                        
                        //$schedule = $scheduler->every(1)
                        //->month(array(intval($date->format('d'))))
                        //->startDate($date->format('Y-m-d'))
                        //->endDate($nextYear->format('Y-m-d'))
                        //->start();
                        

                        $this->logger->info(print_r($schedule,true));
                        //update omise schedule id to customer
                        $omise_schedule_id = $schedule->offsetGet('id');
                        $this->logger->info($omise_schedule_id);
                        $this->customerRepository->updateCustomer(array(
                            "member_id"=>$customer['member_id'],
                            'omise_schedule_id'=>$omise_schedule_id
                        ));
                    }

                    if($invoice['isRenew'] == 1){
                        $this->sendmailThank($invoice['invoice'],true);
                    }else{
                        $this->sendmailThank($invoice['invoice']);
                        $this->sendmailAdmin($invoice['invoice']);
                    }
                    
                break;
                case "failed":
                    $failure_message = $charge->offsetGet('failure_message');
                    $this->logger->error($failure_message);
                    $this->error = $failure_message;
                    $return_status = false;
                    $this->customerRepository->updatePurchaseByInvoice(array(
                        "invoice"=>$invoice['invoice'],
                        "status"=>0,
                        "remark"=>$failure_message
                    ));
                break;
                case "pending" : 
                    $return_status = true;
                    $omise_charge_id = $charge->offsetGet('id');
                    $authorize_uri = $charge->offsetGet('authorize_uri');
                    $this->customerRepository->updatePurchaseByInvoice(array(
                        "invoice"=>$invoice['invoice'],
                        "status"=>0,
                        "authorize_uri"=>$authorize_uri,
                        "omise_charge_id"=>$omise_charge_id
                    ));
                break;
            }
            return $return_status;
        }else{
            $this->logger->error("charge fail::call api omise charge fail");
            $this->error = "charge fail::call api omise charge fail";
            return false;
        }*/
        //End old version
    }

    public function sendmailThank($invoice,$isRenew = false){
        $invoiceData = $this->customerRepository->getPurchaseByInvoice($invoice);
        $member = $this->memberRepository->getMemberById($invoiceData['member_id']);

        $brand = "UNKNOW";
        $last_digits = "-";

        $last_purchase = $this->memberRepository->getLastPurchase($invoiceData['member_id']);
        $customer = $this->customerRepository->getCustomerByMemberId($invoiceData['member_id']);

        /*
        if($invoiceData["subscription_type"] == 2){
            $omise_schedule_id  = $customer['omise_schedule_id'];
            $omise_cust_id = $customer['omise_cust_id'];
            $omise_customer = OmiseCustomer::retrieve($omise_cust_id);
            $default_card = $omise_customer->offsetGet('default_card');
            $card = $omise_customer->getCards()->retrieve($default_card);
            $this->logger->info("card data");
            $this->logger->info(print_r($card,true));
            $brand = $card->offsetGet('brand');
            $last_digits = $card->offsetGet('last_digits');
            $this->logger->info(print_r([$brand,$last_digits],true));
        }else{
            $charge = OmiseCharge::retrieve($invoiceData["omise_charge_id"]);
            $card = $charge->offsetGet('card');
            $this->logger->info(print_r($card,true));
            $brand = $card['brand'];
            $last_digits = $card['last_digits'];
        }
        */
        $omise_cust_id = $customer['omise_cust_id'];
        $omise_customer = OmiseCustomer::retrieve($omise_cust_id);
        $default_card = $omise_customer->offsetGet('default_card');
        $card = $omise_customer->getCards()->retrieve($default_card);
        $this->logger->info("card data");
        $this->logger->info(print_r($card,true));
        $brand = $card->offsetGet('brand');
        $last_digits = $card->offsetGet('last_digits');
        $this->logger->info(print_r([$brand,$last_digits],true));

        
        //$this->logger->info(print_r($charge,true));
        

        $suject = "REALME : Your subscription is successful.";
        if($isRenew){
            $suject = "REALME : Your subscription has been successfully renewed.";
        }

        

        $name = $member->firstname." ".$member->lastname;
        $invoice_date = date_format(date_create($invoiceData['createdtime']),'d M Y');
        $valid_date = date_format(date_create($last_purchase['updatedtime']),'d M Y')." - ".date_format(date_create($customer['expired']),'d M Y');
        //$valid_date = $last_purchase['updatedtime']." - ".$invoiceData['expired'];
        $amount = $invoiceData['amount'];
        $message = <<<EOD
        <div>
        <h4>Dear {$name}</h4>
        <p>Thank you for your subscription. We hope you will enjoy our updates on Realme website.</p>
        </div>
        <p><b>RECEIPT</b></p>
        <dl>
        <dt style="float: left;clear: left;width: 180px;"><b>INVOICE DATE</b><br />{$invoice_date}</dt>
        <dd><b>INVOICE NO.</b><br />{$invoice}</dd>
        </dl>
        <p><b>BILLED TO</b><br />{$brand} ...{$last_digits} <br /> TOTAL <b>¥{$amount}</b><br /> Valid date : {$valid_date}</p>
        <div>
        <br />
        <p>Yours sincerely,</p>
        <p>AYANA MIYAKE<br />Realme Co.,Ltd.<br />www.realme.co.jp</p>
        </div>
EOD;

        $container = $this->app->getContainer();
        $client = new PostmarkClient($container->get('settings')['postmark_token']);
        $sendResult = $client->sendEmail(
            $container->get('settings')['postmark_sender'],
            $member->email,
            $suject,
            $message
        );
        $this->logger->info(print_r($sendResult,true));
        
    }

    private function sendmailAdmin($invoice){
        $invoiceData = $this->customerRepository->getPurchaseByInvoice($invoice);
        $member = $this->memberRepository->getMemberById($invoiceData['member_id']);
        $customer = $this->customerRepository->getCustomerByMemberId($invoiceData['member_id']);

        $date_register = new DateTime($invoiceData['updatedtime']);
        $date_register_format = $date_register->format('l jS F Y \- g.ia');

        $firstname = $member->firstname;
        $lastname = $member->lastname;
        $email = $member->email;
        $mobile = $member->mobile;
        $birthday = date_format(date_create($member->birthday),'jS F Y');
        $isMonthly = ($customer['subscription_type'] == 2) ? "Yes" : "No";

$message = <<<EOD
        <div><p>This is a new subscription from Realme website on {$date_register_format}</p></div>
        <dl>
        <dt style="float: left;clear: left;width: 180px;"><b>FIRSTNAME</b></dt>
        <dd>{$firstname}</dd>
        <dt style="float: left;clear: left;width: 180px;"><b>LASTNAME</b></dt>
        <dd>{$lastname}</dd>
        <dt style="float: left;clear: left;width: 180px;"><b>EMAIL ADDRESS</b></dt>
        <dd>{$email}</dd>
        <dt style="float: left;clear: left;width: 180px;"><b>PHONE NUMBER</b></dt>
        <dd>{$mobile}</dd>
        <dt style="float: left;clear: left;width: 180px;"><b>BIRTHDAY</b></dt>
        <dd>{$birthday}</dd>
        <dt style="float: left;clear: left;width: 180px;"><b>PREFER AUTO-RENEWAL SUBSCRIPTION</b></dt>
        <dd>{$isMonthly}</dd>
        </dl>
        <div>
        <br />
        <p>Yours sincerely,</p>
        <p>AYANA MIYAKE<br />Realme Co.,Ltd.<br />www.realme.co.jp</p>
        </div>
EOD;

        $container = $this->app->getContainer();
        $client = new PostmarkClient($container->get('settings')['postmark_token']);
        $sendResult = $client->sendEmail(
            $container->get('settings')['postmark_sender'],
            "webdev@eqho.com",
            "There is a new subscription from Realme website.",
            $message
        );
        $this->logger->info(print_r($sendResult,true));
    }
}