<?php
namespace App\Domain\SocialLink\Repository;
use PDO;

class SocialLinkRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'ig' => ($data['ig']) ? $data['ig'] : "",
            'tw' => ($data['tw']) ? $data['tw'] : "",
            'fb' => ($data['fb']) ? $data['fb'] : "",
            'line' => ($data['line']) ? $data['line'] : "",
            'yt' => ($data['yt']) ? $data['yt'] : "",
            'yk' => ($data['yk']) ? $data['yk'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE sociallink SET 
                ig=:ig, 
                tw=:tw, 
                fb=:fb, 
                line=:line,
                yt=:yt,
                yk=:yk,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function getById(int $id){
        $sql = "select id,ig,tw,fb,line,yt,yk from sociallink where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('SocialLink not found: %s', $id));
        }
        return $row;
    }
}
