<?php

namespace App\Domain\SocialLink\Service;
use App\Factory\LoggerFactory;
use App\Domain\SocialLink\Data\SocialLinkData;
use App\Domain\SocialLink\Repository\SocialLinkRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class SocialLinkService
{

    private $repository;
    private $logger;

    public function __construct(SocialLinkRepository $repository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger
            ->addFileHandler('SocialLinkService.log')
            ->createInstance('SocialLinkService');
    }
    public function getById(int $id) : SocialLinkData {
        $data = $this->repository->getById($id);
        $return = new SocialLinkData();
        $return->id = $data['id'];
        $return->ig = $data['ig'];
        $return->tw = $data['tw'];
        $return->fb = $data['fb'];
        $return->line = $data['line'];
        $return->yt = $data['yt'];
        $return->yk = $data['yk'];
        return $return;
    }

    public function updateSocialLink(int $id,array $data){
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "ig"=>$olddata->ig,
            "tw"=>$olddata->tw,
            "fb"=>$olddata->fb,
            "line"=>$olddata->line,
            "yt"=>$olddata->yt,
            "yk"=>$olddata->yk
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        $this->repository->update($id,$oldarray);
    }
    
}