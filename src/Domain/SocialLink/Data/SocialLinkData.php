<?php
namespace App\Domain\SocialLink\Data;
final class SocialLinkData
{
    public $id;
    public $ig;
    public $tw;
    public $fb;
    public $line;
    public $yt;
    public $yk;
}