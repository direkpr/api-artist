<?php
namespace App\Domain\News\Repository;
use PDO;

class NewsRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'release_date' => ($data['release_date']) ? $data['release_date'] : date("Y-m-d"),
            'category_id' => ($data['category_id']) ? $data['category_id'] : 1,
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'shareContent'=>($data['shareContent']) ? $data['shareContent'] : "",
            'imgShare'=>($data['imgShare']) ? $data['imgShare'] : "",
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO news SET 
                release_date=:release_date, 
                category_id=:category_id, 
                isActive=:isActive, 
                datasearch=:datasearch,
                shareContent=:shareContent,
                imgShare=:imgShare,
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'release_date' => ($data['release_date']) ? $data['release_date'] : date("Y-m-d"),
            'category_id' => ($data['category_id']) ? $data['category_id'] : 1,
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'shareContent'=>($data['shareContent']) ? $data['shareContent'] : "",
            'imgShare'=>($data['imgShare']) ? $data['imgShare'] : "",
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE news SET 
                release_date=:release_date, 
                category_id=:category_id, 
                isActive=:isActive, 
                shareContent=:shareContent,
                imgShare=:imgShare,
                datasearch=:datasearch,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function get_total_count($search) : int 
    {
        $datasearch = $search;
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
        $sql = "select count(*) as num from news where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $datasearch = $param['search'];
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
            $where = "where a.datasearch like '%{$search}%'";
        }
        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,b.title,b.content,a.release_date,a.category_id,a.createdtime,a.updatedtime,a.isActive,a.shareContent,a.imgShare FROM `news` a left JOIN news_txt b on a.id = b.news_id and b.lg = '{$lg}' {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,release_date,category_id,isActive,shareContent,imgShare from news where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('News not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `news` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /** method for front-end */
    public function get_active_news($param){
        //var_dump($param);
        $limit = "";
        if(isset($param['limit']) && isset($param['offset'])){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }
        //echo "limit=>{$limit}";
        $condition = "";
        if(isset($param['category']) && $param['category'] != 0){
            $condition = " and category_id = ".$param['category'];
        }
        $sql = "select id,release_date,category_id,shareContent,imgShare from news where isActive = 1 {$condition} order by release_date desc {$limit}";

        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function get_active_count($param){
        $condition = "";
        if(isset($param['category']) && $param['category'] != 0){
            $condition = " and category_id = ".$param['category'];
        }
        $sql = "select count(*) as num from news where isActive = 1 {$condition}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
}
