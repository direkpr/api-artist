<?php
namespace App\Domain\News\Repository;
use PDO;
use App\Factory\LoggerFactory;

class NewsTextRepository
{
    private $connection;
    private $logger;
    public function __construct(PDO $connection,LoggerFactory $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
        $this->logger = $logger
            ->addFileHandler('NewsTextRepository.log')
            ->createInstance('NewsTextRepository');
    }
    public function insert(array $data): int
    {
        $row = [
            'news_id' => $data['news_id'],
            'title' => ($data['title']) ? $data['title'] : "",
            'content' => ($data['content']) ? base64_encode($data['content']) : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "INSERT INTO news_txt SET 
                news_id=:news_id, 
                title=:title, 
                content=:content,
                lg=:lg";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'news_id' => $data['news_id'],
            'title' => ($data['title']) ? $data['title'] : "",
            'content' => ($data['content']) ? base64_encode($data['content']) : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "UPDATE news_txt SET 
                news_id=:news_id, 
                title=:title, 
                content=:content,
                lg=:lg WHERE id =:id";
        $this->connection->prepare($sql)->execute($row);
    }
    public function getByNewsId(int $id) {
        $sql = "select id,news_id,title,content,lg from news_txt where news_id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        
        foreach ($rows as &$value) {
            $value['content'] = base64_decode($value['content']);
        }
        //$this->logger->info(print_r($rows,true));
        if (!$rows) {
            $rows = [];
        }
        return $rows;
    }
    public function deleteByNewsId(int $id){
        $sql = "DELETE FROM `news_txt` WHERE `news_id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
