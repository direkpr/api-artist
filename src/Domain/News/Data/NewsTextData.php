<?php
namespace App\Domain\News\Data;
final class NewsTextData
{
    public $id;
    public $news_id;
    public $title;
    public $content;
    public $lg;
}