<?php
namespace App\Domain\News\Data;
final class NewsData
{
    public $id;
    public $release_date;
    public $category_id;
    public $isActive;
    public $texts;
    public $shareContent;
    public $imgShare;
}