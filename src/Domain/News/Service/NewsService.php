<?php

namespace App\Domain\News\Service;
use App\Factory\LoggerFactory;
use App\Domain\News\Data\NewsData;
use App\Domain\News\Repository\NewsRepository;
use App\Domain\News\Repository\NewsTextRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class NewsService
{

    private $repository;
    private $textRepository;
    private $langsRepository;
    private $logger;

    public function __construct(NewsRepository $repository,NewsTextRepository $textRepository,LangsRepository $langsRepository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->textRepository = $textRepository;
        $this->langsRepository = $langsRepository;

        $this->logger = $logger
            ->addFileHandler('NewsService.log')
            ->createInstance('NewsService');
    }
    public function deleteNews(int $id){
        $this->repository->delete($id);
        $this->textRepository->deleteByNewsId($id);
    }
    public function getById(int $id) : NewsData {
        $data = $this->repository->getById($id);
        $return = new NewsData();
        $return->id = $data['id'];
        $return->release_date = $data['release_date'];
        $return->category_id = $data['category_id'];
        $return->isActive = $data['isActive'];
        $return->texts = $this->textRepository->getByNewsId((int)$data['id']);
        $return->shareContent = $data['shareContent'];
        $return->imgShare = $data['imgShare'];

        return $return;
    }
    public function createNews(array $data): int
    {
        // Input validation
        $this->validateNewNews($data);
        $datasearch = '';
        foreach($data['texts'] as $text){
            $datasearch .= $text['title'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            foreach($data['texts'] as $text){
                $text['news_id'] = $insert_id;
                if($this->validateTextNews($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
        return $insert_id;
    }
    public function updateNews(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "release_date"=>$olddata->release_date,
            "category_id"=>$olddata->category_id,
            "isActive"=>$olddata->isActive,
            "texts"=>$olddata->texts,
            "shareContent"=>$olddata->shareContent,
            "imgShare"=>$olddata->imgShare
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        //$this->logger->info(print_r($oldarray,true));
        $this->validateNewNews($oldarray);
        $datasearch = '';
        foreach($oldarray['texts'] as $text){
            $datasearch .= $text['title'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $oldarray['datasearch'] = $datasearch;
        $this->repository->update($id,$oldarray);
        foreach($oldarray['texts'] as $text){
            if(isset($text['id'])){
                //update
                $text['news_id'] = $id;
                if($this->validateTextNews($text)){
                    $this->textRepository->update($text['id'],$text);
                }
            }else{
                $text['news_id'] = $id;
                if($this->validateTextNews($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $lang = $this->langsRepository->get_default_lang();
        $params['lg'] = $lang->code;
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    public function get_active_count($params) : int {
        return $this->repository->get_active_count($params);
    }
    public function get_active_news($params){
        $data = $this->repository->get_active_news($params);
        if(empty($data) || is_null($data))
            $data = [];

        foreach($data as $key => $val){
            $data[$key]['texts'] = $this->textRepository->getByNewsId($val['id']);
        }
        return $data;
    }
    private function validateTextNews(array $data): bool {
        if(empty($data['lg'])){
            return false;
        }
        if(empty($data['title'])){
            return false;
        }
        return true;
    }

    private function validateNewNews(array $data): void
    {
        $errors = [];

        if (empty($data['release_date'])) {
            $errors['release_date'] = 'Input required';
        }

        if (empty($data['category_id'])) {
            $errors['category_id'] = 'Input required';
        }

        if(empty($data['texts'])){
            $errors['texts'] = 'Input required';
        }

        $lang = $this->langsRepository->get_default_lang();
        if(!$lang){
            $errors['lang'] = "default lang not found";
            throw new ValidationException('default lang not found', $errors);
        }
        //var_dump($lang['code']);
        $default_text = null;
        foreach($data['texts'] as $text){
            if($lang['code'] == $text['lg']){
                $default_text = $text;
            }
        }
        //var_dump($default_text);
        if(is_null($default_text)){
            $errors['texts'] = 'default text not found';
            throw new ValidationException('default text not found', $errors);
        }
        if(empty($default_text['title'])){
            $errors['text->title'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }
}