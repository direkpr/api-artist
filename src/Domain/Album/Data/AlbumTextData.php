<?php
namespace App\Domain\Album\Data;
final class AlbumTextData
{
    public $id;
    public $album_id;
    public $title;
    public $lg;
}