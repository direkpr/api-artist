<?php
namespace App\Domain\Album\Data;
final class AlbumData
{
    public $id;
    public $year;
    public $coverUrl;
    public $isActive;
    public $texts;
}