<?php
namespace App\Domain\Album\Repository;
use PDO;

class AlbumTextRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'album_id' => $data['album_id'],
            'title' => ($data['title']) ? $data['title'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "INSERT INTO albums_txt SET 
                album_id=:album_id, 
                title=:title, 
                lg=:lg";
        $this->connection->prepare($sql)->execute($row);
        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'album_id' => $data['album_id'],
            'title' => ($data['title']) ? $data['title'] : "",
            'lg' => ($data['lg']) ? $data['lg'] : "en"
        ];
        //var_dump($row);


        $sql = "UPDATE albums_txt SET 
                album_id=:album_id, 
                title=:title, 
                lg=:lg WHERE id =:id";
        $this->connection->prepare($sql)->execute($row);
    }
    public function getByAlbumId(int $id) {
        $sql = "select id,album_id,title,lg from albums_txt where album_id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        if (!$rows) {
            $rows = [];
        }
        return $rows;
    }
    public function deleteByAlbumId(int $id){
        $sql = "DELETE FROM `albums_txt` WHERE `album_id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}
