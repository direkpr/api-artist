<?php
namespace App\Domain\Album\Repository;
use PDO;

class AlbumRepository
{
    private $connection;
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    public function insert(array $data): int
    {
        $row = [
            'coverUrl' => ($data['coverUrl']) ? $data['coverUrl'] : "",
            'year' => ($data['year']) ? $data['year'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'createdtime' => date("Y-m-d H:i:s"),
            'updatedtime' => date("Y-m-d H:i:s")
        ];

        $sql = "INSERT INTO albums SET 
                coverUrl=:coverUrl, 
                year=:year, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime,
                createdtime=:createdtime;";

        $this->connection->prepare($sql)->execute($row);

        return (int)$this->connection->lastInsertId();
    }
    public function update($id,$data){
        $row = [
            'id' => $id,
            'coverUrl' => ($data['coverUrl']) ? $data['coverUrl'] : "",
            'year' => ($data['year']) ? $data['year'] : "",
            'isActive' => ($data['isActive'] == 1) ? 1 : 0,
            'datasearch'=>($data['datasearch']) ? $data['datasearch'] : "",
            'updatedtime' => date("Y-m-d H:i:s")
        ];
        $sql = "UPDATE albums SET 
                coverUrl=:coverUrl, 
                year=:year, 
                isActive=:isActive, 
                datasearch=:datasearch,
                updatedtime=:updatedtime WHERE id=:id;";

        $this->connection->prepare($sql)->execute($row);
    }
    public function get_total_count($search) : int 
    {
        $datasearch = $search;
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
        $sql = "select count(*) as num from albums where datasearch like '%%{$search}%%'";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        return intval($row['num']);
    }
    public function get_data($param){
        $lg = ($param['lg']) ? $param['lg'] : "en";
        $where = "";
        if(isset($param['search']) && strlen($param['search']) > 0){
            $datasearch = $param['search'];
            $datasearch = strtolower($datasearch);
            $datasearch = preg_replace('/\s+/', '', $datasearch);
            $search = $datasearch;
            $where = "where a.datasearch like '%{$search}%'";
        }
        $orderby = "";
        if(isset($param['orderby'])){
            $orderby = "order by ".$param['orderby']." ".$param['orderdirection'];
        }
        $limit = "";
        if(isset($param['limit']) && $param['offset']){
            $limit = "limit ".$param['limit']." offset ".$param['offset'];
        }

        $sql = "SELECT a.id,b.title,a.coverUrl,a.year,a.createdtime,a.updatedtime,a.isActive FROM `albums` a left JOIN albums_txt b on a.id = b.album_id and b.lg = '{$lg}' {$where} {$orderby} {$limit} ";
        
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
    public function getById(int $id){
        $sql = "select id,coverUrl,year,isActive from albums where id = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $row = $statement->fetch();
        if (!$row) {
            throw new DomainException(sprintf('Goods not found: %s', $id));
        }
        return $row;
    }
    public function delete(int $id){
        $sql = "DELETE FROM `albums` WHERE `id` = {$id}";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /** method for front-end */
    public function get_active_album(){
        $sql = "select id,coverUrl,year from albums where isActive = 1 order by year desc";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $rows = $statement->fetchAll();
        return $rows;
    }
}
