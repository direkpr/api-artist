<?php

namespace App\Domain\Album\Service;
use App\Factory\LoggerFactory;
use App\Domain\Album\Data\AlbumData;
use App\Domain\Album\Repository\AlbumRepository;
use App\Domain\Album\Repository\AlbumTextRepository;
use App\Domain\Langs\Repository\LangsRepository;
use App\Exception\ValidationException;

/**
 * Service.
 */
final class AlbumService
{

    private $repository;
    private $textRepository;
    private $langsRepository;
    private $logger;

    public function __construct(AlbumRepository $repository,AlbumTextRepository $textRepository,LangsRepository $langsRepository, LoggerFactory $logger)
    {
        $this->repository = $repository;
        $this->textRepository = $textRepository;
        $this->langsRepository = $langsRepository;

        $this->logger = $logger
            ->addFileHandler('AlbumService.log')
            ->createInstance('AlbumService');
    }
    public function deleteAlbum(int $id){
        $this->repository->delete($id);
        $this->textRepository->deleteByAlbumId($id);
    }
    public function getById(int $id) : AlbumData {
        $data = $this->repository->getById($id);
        $return = new AlbumData();
        $return->id = $data['id'];
        $return->coverUrl = $data['coverUrl'];
        $return->year = $data['year'];
        $return->isActive = $data['isActive'];
        $return->texts = $this->textRepository->getByAlbumId((int)$data['id']);
        return $return;
    }
    public function createAlbum(array $data): int
    {
        // Input validation
        $this->validateNewAlbum($data);
        $datasearch = '';
        foreach($data['texts'] as $text){
            $datasearch .= $text['title'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $data['datasearch'] = $datasearch;

        $insert_id = $this->repository->insert($data);
        if($insert_id){
            foreach($data['texts'] as $text){
                $text['album_id'] = $insert_id;
                if($this->validateTextAlbum($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
        return $insert_id;
    }
    public function updateAlbum(int $id,array $data){
        if(isset($data['isActive']) && $data['isActive'] == 1){
            $data['isActive'] = 1;
        }else{
            $data['isActive'] = 0;
        }
        $olddata = $this->getById($id);
        $oldarray = array(
            "id"=>$olddata->id,
            "coverUrl"=>$olddata->coverUrl,
            "year"=>$olddata->year,
            "isActive"=>$olddata->isActive,
            "texts"=>$olddata->texts
        );
        foreach($data as $key => $value){
            $oldarray[$key] = $value;
        }
        //$this->logger->info(print_r($oldarray,true));
        $this->validateNewAlbum($oldarray);
        $datasearch = '';
        foreach($oldarray['texts'] as $text){
            $datasearch .= $text['title'];
        }
        $datasearch = strtolower($datasearch);
        $datasearch = preg_replace('/\s+/', '', $datasearch);
        $oldarray['datasearch'] = $datasearch;
        $this->repository->update($id,$oldarray);
        foreach($oldarray['texts'] as $text){
            if(isset($text['id'])){
                //update
                $text['album_id'] = $id;
                if($this->validateTextAlbum($text)){
                    $this->textRepository->update($text['id'],$text);
                }
            }else{
                $text['album_id'] = $id;
                if($this->validateTextAlbum($text)){
                    $this->textRepository->insert($text);
                }
            }
        }
    }
    public function get_total_count($params) : int {
        $search = '';
        if(isset($params['search'])){
            $search = $params['search'];
        }
        return $this->repository->get_total_count($search);
    }
    public function get_data($params){
        $lang = $this->langsRepository->get_default_lang();
        $params['lg'] = $lang->code;
        $data = $this->repository->get_data($params);
        if(empty($data) || is_null($data))
            $data = [];
        return $data;
    }
    private function validateTextAlbum(array $data): bool {
        if(empty($data['lg'])){
            return false;
        }
        if(empty($data['title'])){
            return false;
        }
        return true;
    }

    private function validateNewAlbum(array $data): void
    {
        $errors = [];

        if (empty($data['coverUrl'])) {
            $errors['coverUrl'] = 'Input required';
        }

        if (empty($data['year'])) {
            $errors['year'] = 'Input required';
        }

        if(empty($data['texts'])){
            $errors['texts'] = 'Input required';
        }

        $lang = $this->langsRepository->get_default_lang();
        if(!$lang){
            $errors['lang'] = "default lang not found";
            throw new ValidationException('default lang not found', $errors);
        }
        //var_dump($lang['code']);
        $default_text = null;
        foreach($data['texts'] as $text){
            if($lang['code'] == $text['lg']){
                $default_text = $text;
            }
        }
        //var_dump($default_text);
        if(is_null($default_text)){
            $errors['texts'] = 'default text not found';
            throw new ValidationException('default text not found', $errors);
        }
        if(empty($default_text['title'])){
            $errors['text->title'] = 'Input required';
        }

        if ($errors) {
            throw new ValidationException('Please check your input', $errors);
        }
    }
}