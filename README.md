### How to deploy artist's projects on server
All sorce code 
- [API](https://bitbucket.org/direkpr/api-artist/src/master/)
- [CMS](https://bitbucket.org/direkpr/cms-artist/src/master/)
- [SITE](https://bitbucket.org/direkpr/site-artist/src/master/)

### 1st STEP
Configuring Nginx Server - Path & Structure

- htdocs
    - /
    - /cms/
    - /php/

Config nginx.conf 

`server {
    location / {
        try_files $uri /index.html;
    }
    location ~ ^/cms {
        try_files $uri $uri/ /cms/index.html;
    }
}`

### 2nd STEP
Config & deploy api to server (develope with `slim framework 4`)

1. Download api repo `git clone https://direkpr@bitbucket.org/direkpr/api-artist.git`  & run `php composer.phar install`
2. Import `db_realme.sql` to MySQL database server
3. Edit database config in file `config/settings.php` on line 46:

    >$settings['db'] = [

4. Config `POSTMARK` sendmail provider in file `config/settings.php` on line 26,27:

    >$settings['postmark_token']

    >$settings['postmark_sender']

5. Change your domain in file `config/settings.php` on line 28:

    >$settings['img_link'] = "https://[yourdomain]/php"

6. Change `OMISE_PUBLIC_KEY` & `OMISE_SECRET_KEY` in file `config/bootstrap.php` on line 7,8:

    >define('OMISE_PUBLIC_KEY', 'pkey_test_5ak536wym8ha4o9fc3a');

    >define('OMISE_SECRET_KEY', 'skey_test_5ak536wz69i4wwhfgfj');

7. Change your domain in file `config/bootstrap.php` on line 14,15:

    >define('RETURN_URI',"https://[yourdomain]/charge/");

    >define('FORGOT_URI',"https://[yourdomain]/forgot-password/");

8. You can change ONETIME or MONTHLY fee in `config/bootstrap.php` on line 11,12
9. Change permision `777` to `logs`,`uploads` and `tmp`
10. Test & run in local with comand `php -S localhost:8080 -t public`
11. If every this is ok in local server, upload all files & folders to `/php/` on production server. (include `.htaccess` & `vendor`  in root directory )


### 3rd STEP
Config & deploy cms to server (develope with `reactjs framework`)

1. Download api repo `git clone https://direkpr@bitbucket.org/direkpr/cms-artist.git`  & run `cd cms-artist & npm install`
2. Change `apiUrl` & `imgUrl` to your api's domain in file `src/config.js` on line 2,4 :

    >apiUrl : "https://[yourdomain]/php",

    >imgUrl : "https://[yourdomain]/php",

3. Test & run on local with command `npm start` and you can test on browser with url `http://localhost:3000/cms/`
4. Build project for production with command `npm build` and copy all files in `build` directory to `/cms/` on your server 

### 4th STEP
Config & deploy site to server  (develope with `reactjs framework`)

1. Download api repo `git clone https://direkpr@bitbucket.org/direkpr/site-artist.git`  & run `cd site-artist & npm install`
2. Change `apiUrl` , `imgUrl` & `shareLinkUrl`  to your api's domain in file `src/config.js` on line 11,13,16 :

    >apiUrl : "https://[yourdomain]/php",

    >imgUrl : "https://[yourdomain]/php",

    >shareLinkUrl : "http://[yourdomain]/php/share/news/"

3. Change OMISE public key in `public/index.html` on line 32:

    >Omise.setPublicKey("pkey_test_5ak536wym8ha4o9fc3a");

4. Test & run on local with command `npm start` and you can test on browser with url `http://localhost:3000`
5. Build project for production with command `npm build` and copy all files in `build` directory to `/` on your server 