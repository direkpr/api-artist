-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 16, 2021 at 08:23 AM
-- Server version: 8.0.15
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_realme`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `coverUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` int(11) NOT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `datasearch`, `year`, `coverUrl`, `isActive`, `createdtime`) VALUES
(1, '', 2001, '/get-file-upload?f=82c28813eaacdc33.png', 1, '2020-09-14 14:37:12'),
(2, 'alum2', 2002, '/get-file-upload?f=1d85432d7888e857.jpg', 1, '2020-09-24 17:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `albums_txt`
--

CREATE TABLE `albums_txt` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lg` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `albums_txt`
--

INSERT INTO `albums_txt` (`id`, `album_id`, `title`, `lg`) VALUES
(1, 1, 'Album1x', 'en'),
(2, 2, 'Alum 2', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `street1` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street2` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `fname`, `lname`, `email`, `mobile`, `company`, `country`, `street1`, `street2`, `city`, `state`, `zipcode`, `subject`, `message`, `datasearch`, `createdtime`) VALUES
(1, 'direk1', 'prama', 'alek.pr@gmail.com', '0814414183', '', 'Thailand', 'Home 1', '', 'BKK', 'Bangkok', '10250', 'Test subject', 'Test message', 'direkpramaalek.pr@gmail.com0814414183testsubject', '2020-09-18 14:18:29'),
(2, 'direk2', 'prama', 'alek.pr@gmail.com', '0814414183', '', 'Thailand', 'Home 1', '', 'BKK', 'Bangkok', '10250', 'Test subject', 'Test message', 'direkpramaalek.pr@gmail.com0814414183testsubject', '2020-09-18 14:23:27'),
(3, 'direk', 'prama', 'alek.pr@gmail.com', '0814414183', '', 'Thailand', 'Home 1', '', 'BKK', 'Bangkok', '10250', 'Test subject', 'Test message', 'direkpramaalek.pr@gmail.com0814414183testsubject', '2020-09-18 14:38:16'),
(4, 'direk', 'prama', 'alek.pr@gmail.com', '0814414183', '', 'Thailand', 'Home 1', '', 'BKK', 'Bangkok', '10250', 'Test subject', 'Test message', 'direkpramaalek.pr@gmail.com0814414183testsubject', '2020-09-18 14:48:22'),
(6, 'Direk', 'Prama', 'alek.pr@gmail.com', '0814414183', 'CLOUD9', 'Thailand', '764/376 Villette Lite', 'Pattanakarn 38, Saun Luang', 'Saun Luang', 'กรุงเทพมหานคร', '10250', 'test subject', 'test message', 'direkpramaalek.pr@gmail.com0814414183cloud9testsubject', '2020-09-24 13:46:12'),
(7, 'Direk', 'Prama', 'alek.pr@gmail.com', '0814414183', 'CLOUD9', 'Thailand', '764/376 Villette Lite', 'Pattanakarn 38, Saun Luang', 'Saun Luang', 'กรุงเทพมหานคร', '10250', 'test subject', 'test mesage', 'direkpramaalek.pr@gmail.com0814414183cloud9testsubject', '2020-09-24 13:48:10'),
(8, 'Direk', 'Prama', 'alek.pr@gmail.com', '0814414183', 'CLOUD9', 'Thailand', '764/376 Villette Lite', 'Pattanakarn 38, Saun Luang', 'Saun Luang', 'กรุงเทพมหานคร', '10250', 'test 2', 'test message 2', 'direkpramaalek.pr@gmail.com0814414183cloud9test2', '2020-09-24 13:52:31'),
(9, 'Direk', 'Prama', 'alek.pr@gmail.com', '0814414183', 'CLOUD9', 'Thailand', '764/376 Villette Lite', 'Pattanakarn 38, Saun Luang', 'Saun Luang', 'กรุงเทพมหานคร', '10250', 'xxx', 'xxxxyyy', 'direkpramaalek.pr@gmail.com0814414183cloud9xxx', '2020-09-24 13:53:47'),
(10, 'Direk', 'Prama', 'alek.pr@gmail.com', '0814414183', 'CLOUD9', 'Thailand', '764/376 Villette Lite', 'Pattanakarn 38, Saun Luang', 'Saun Luang', 'กรุงเทพมหานคร', '10250', 'test', 'test', 'direkpramaalek.pr@gmail.com0814414183cloud9test', '2020-09-29 15:08:51');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `omise_cust_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `omise_schedule_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscription_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0 = Guest, 1 = Subscription, 2 = monthly subscription',
  `expired` datetime DEFAULT NULL,
  `next_billdate` datetime DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `member_id`, `omise_cust_id`, `omise_schedule_id`, `subscription_type`, `expired`, `next_billdate`, `createdtime`) VALUES
(4, 13, 'cust_test_5lpk4pbw6z0b8o1og9z', NULL, 1, '2020-11-30 18:45:19', '2020-11-30 18:45:19', '2020-10-30 18:30:11'),
(19, 28, 'cust_test_5o0tu8f1av18misanx3', 'schd_test_5o0tudcplt0o8omnnsa', 2, '2021-06-02 00:00:00', '2021-06-02 00:00:00', '2021-05-31 16:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `ebirthday_cards`
--

CREATE TABLE `ebirthday_cards` (
  `id` int(11) NOT NULL,
  `imgUrl` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(3) NOT NULL DEFAULT '1',
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ebirthday_sends`
--

CREATE TABLE `ebirthday_sends` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `createdtime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ebirthday_sends`
--

INSERT INTO `ebirthday_sends` (`id`, `member_id`, `birthday`, `createdtime`) VALUES
(1, 23, '1982-12-17', '2020-12-17 14:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE `goods` (
  `id` int(11) NOT NULL,
  `imgUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(3) NOT NULL DEFAULT '1',
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `goods`
--

INSERT INTO `goods` (`id`, `imgUrl`, `link`, `isActive`, `datasearch`, `createdtime`) VALUES
(2, '/get-file-upload?f=78881652cdec6504.png', 'test2', 1, 'test2test2test2test', '2020-09-10 16:50:41'),
(3, '/get-file-upload?f=4eca116189954a6a.png', 'ssss1', 1, '', '2020-09-10 17:54:59');

-- --------------------------------------------------------

--
-- Table structure for table `goods_txt`
--

CREATE TABLE `goods_txt` (
  `id` int(11) NOT NULL,
  `good_id` int(11) NOT NULL,
  `good_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `good_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `good_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `good_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lg` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `goods_txt`
--

INSERT INTO `goods_txt` (`id`, `good_id`, `good_name`, `good_type`, `good_code`, `good_price`, `lg`) VALUES
(2, 2, 'test2', 'test2', 'test2', 'test', 'en'),
(3, 3, 'testjas1', 'tetsssss1', 'dddddd1', '1ssss1', 'en'),
(4, 3, 'aaa1', 'dd1', 'aaa1', 'ff1', 'jp');

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

CREATE TABLE `langs` (
  `id` int(11) NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isDefault` tinyint(3) NOT NULL DEFAULT '0',
  `isActive` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`id`, `code`, `title`, `isDefault`, `isActive`) VALUES
(1, 'en', 'English', 1, 1),
(2, 'jp', 'Japanese', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `marketing_scripts`
--

CREATE TABLE `marketing_scripts` (
  `id` int(11) NOT NULL,
  `in_head_tag` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `open_body_tag` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `close_body_tag` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marketing_scripts`
--

INSERT INTO `marketing_scripts` (`id`, `in_head_tag`, `open_body_tag`, `close_body_tag`, `createdtime`) VALUES
(1, ' ', ' ', ' ', '2020-09-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `roles` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '["customer"]',
  `displayname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0=not success, 1=successful'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `firstname`, `lastname`, `email`, `mobile`, `birthday`, `roles`, `displayname`, `createdtime`, `payment_status`) VALUES
(1, 'direk', 'prama', 'alek.pr@gmail.com', '', NULL, '[\"admin\"]', NULL, '2020-09-09 14:18:39', 1),
(13, 'Atchara', 'Tantrakarn', 'atchara@cloud9worldwide.com', '0863484885', '2014-06-05', '[\"customer\"]', NULL, '2020-10-30 18:30:11', 1),
(28, 'Direk', 'Prama', 'direk@cloud9worldwide.com', '0814414183', '1982-07-10', '[\"customer\"]', NULL, '2021-05-31 16:11:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_credential`
--

CREATE TABLE `member_credential` (
  `id` int(11) NOT NULL,
  `memberId` int(11) NOT NULL,
  `password` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member_credential`
--

INSERT INTO `member_credential` (`id`, `memberId`, `password`, `createdtime`) VALUES
(1, 1, 'b30c6b0910b270824978f4c9ae15cd75', '2020-09-09 14:18:39'),
(2, 3, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-20 11:44:59'),
(3, 4, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-22 16:30:00'),
(4, 5, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-22 16:35:51'),
(5, 6, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-22 16:40:11'),
(6, 7, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-22 16:42:52'),
(7, 8, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-22 16:50:54'),
(8, 9, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-22 17:04:32'),
(9, 10, '1bbd886460827015e5d605ed44252251', '2020-10-22 17:09:02'),
(10, 11, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-28 10:37:26'),
(11, 12, 'b30c6b0910b270824978f4c9ae15cd75', '2020-10-30 14:21:56'),
(12, 13, 'fcea920f7412b5da7be0cf42b8c93759', '2020-10-30 18:30:11'),
(13, 14, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 10:18:28'),
(14, 15, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 10:30:12'),
(15, 16, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 10:34:28'),
(16, 17, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 14:38:53'),
(17, 18, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 14:52:24'),
(18, 19, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 15:01:38'),
(19, 20, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-20 15:07:27'),
(20, 21, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-23 14:39:10'),
(21, 22, 'b30c6b0910b270824978f4c9ae15cd75', '2020-11-25 10:59:37'),
(27, 28, 'b30c6b0910b270824978f4c9ae15cd75', '2021-05-31 16:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `member_social`
--

CREATE TABLE `member_social` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `social_id` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `metetags`
--

CREATE TABLE `metetags` (
  `id` int(11) NOT NULL,
  `title` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `metetags`
--

INSERT INTO `metetags` (`id`, `title`, `keywords`, `description`, `createdtime`) VALUES
(1, 'REALMEx', 'realmesx', 'realme\'s web sitesssx', '2020-09-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `release_date` date NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isActive` tinyint(3) NOT NULL DEFAULT '1',
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `imgShare` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shareContent` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `release_date`, `category_id`, `datasearch`, `isActive`, `createdtime`, `imgShare`, `shareContent`) VALUES
(1, '2020-09-10', 3, '', 1, '2020-09-15 16:57:16', NULL, NULL),
(2, '2020-09-22', 2, '', 1, '2020-09-22 14:16:03', NULL, NULL),
(3, '2020-09-22', 2, '', 1, '2020-09-22 14:16:20', NULL, NULL),
(4, '2020-09-22', 1, '', 1, '2020-09-22 14:16:42', NULL, NULL),
(5, '2020-09-21', 1, '', 1, '2020-09-22 14:18:58', NULL, NULL),
(6, '2020-10-30', 2, 'test', 1, '2020-10-30 13:44:43', '/get-file-upload?f=f0bf392a92f342fa.png', 'test'),
(7, '2020-10-30', 2, 'testss', 1, '2020-10-30 13:57:30', '/get-file-upload?f=46f37fb3ec191c72.png', 'ssss');

-- --------------------------------------------------------

--
-- Table structure for table `news_txt`
--

CREATE TABLE `news_txt` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `lg` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_txt`
--

INSERT INTO `news_txt` (`id`, `news_id`, `lg`, `title`, `content`) VALUES
(1, 1, 'en', 'test', 'PHA+dGVzdCBqYTwvcD4='),
(2, 1, 'jp', 'title-jp', 'PHA+dGVzdCBjb250ZW50IGpwIGphPC9wPg=='),
(3, 2, 'en', 'test2', 'PHA+dGVzdCAyPC9wPg=='),
(4, 3, 'en', 'test 3', 'PHA+dGVzdCAzPC9wPg=='),
(5, 4, 'en', 'test 4', 'PHA+dGVzdCA0PC9wPg=='),
(6, 5, 'en', 'test 5', 'PHA+dGVzdCA1PC9wPg=='),
(7, 5, 'jp', 'test 5 - jp', 'PHA+dGVzdCA1IGpwIGNvbnRlbnQ8L3A+'),
(8, 4, 'jp', 'test 4 - jp', 'PHA+dGVzdCA0IGpwIGNvbnRlbnQ8L3A+'),
(9, 3, 'jp', 'test 3 - jp', 'PHA+dGVzdCAzIGpwIGNvbnRlbnQ8L3A+'),
(10, 2, 'jp', 'test 2 - jp ', 'PHA+dGVzdCAyIGpwIGNvbnRlbnQ8L3A+'),
(11, 6, 'en', 'test', 'PHA+dGVzdDwvcD4='),
(12, 7, 'en', 'testss', 'PHA+c3NzczwvcD4=');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `id` int(11) NOT NULL,
  `thumUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isActive` tinyint(3) NOT NULL DEFAULT '1',
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`id`, `thumUrl`, `imgUrl`, `link`, `isActive`, `datasearch`, `createdtime`) VALUES
(1, '/get-file-upload?f=2ac614189983c180.png', '/get-file-upload?f=a9c7a2178bb63845.jpg', 'test', 1, '', '2020-09-11 16:41:50');

-- --------------------------------------------------------

--
-- Table structure for table `photo_txt`
--

CREATE TABLE `photo_txt` (
  `id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lg` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `photo_txt`
--

INSERT INTO `photo_txt` (`id`, `photo_id`, `name`, `price`, `lg`) VALUES
(1, 1, 'test', 'test', 'en'),
(2, 1, 'test', 'ssss', 'jp');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL,
  `invoice` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `member_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'JPY',
  `status` tinyint(3) NOT NULL DEFAULT '-1' COMMENT '-1=not charge,0=pedding, 1=complete, 2= failed, 3= expired, 4= reversed',
  `remark` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `authorize_uri` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `omise_charge_id` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isRenew` tinyint(3) NOT NULL DEFAULT '0',
  `subscription_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `invoice`, `member_id`, `amount`, `currency`, `status`, `remark`, `authorize_uri`, `omise_charge_id`, `createdtime`, `isRenew`, `subscription_type`) VALUES
(1, '2020102200001', 10, 500, 'JPY', 1, 'Purchase subscription memberId #10', 'https://api.omise.co/payments/paym_test_5lmek3wngns5j51vmwn/authorize', 'chrg_test_5lmek3wihfb33goeo6a', '2020-10-22 17:09:02', 0, 1),
(2, '2020102800002', 11, 500, 'JPY', 1, 'Purchase subscription memberId #11', 'https://api.omise.co/payments/paym_test_5lon7ps45fycm21do9n/authorize', 'chrg_test_5lon7ps1n5pjmwmdfz0', '2020-10-28 10:37:26', 0, 1),
(3, '2020102900003', 10, 500, 'JPY', 1, 'Purchase subscription memberId #10', 'https://api.omise.co/payments/paym_test_5lp38477ufsezf90rtu/authorize', 'chrg_test_5lp38474l7ilwifo0bt', '2020-10-29 13:54:43', 0, 1),
(4, '2020102900004', 10, 500, 'JPY', 1, 'Purchase subscription memberId #10', 'https://api.omise.co/payments/paym_test_5lp3g4hzzpwtpiq1kmr/authorize', 'chrg_test_5lp3g4hwyhg0pyvq3ux', '2020-10-29 14:17:39', 1, 1),
(5, '2020103000005', 12, 500, 'JPY', 0, 'Purchase subscription memberId #12', '', '', '2020-10-30 14:21:57', 0, 1),
(6, '2020103000006', 10, 500, 'JPY', 1, 'Purchase subscription memberId #10', 'https://api.omise.co/payments/paym_test_5lpi5nnvgx3d8ciwvp7/authorize', 'chrg_test_5lpi5nnrcykhnmbiqkk', '2020-10-30 15:21:50', 1, 1),
(7, '2020103000007', 13, 500, 'JPY', 2, '3d secure is requested but card is not enrolled', '', '', '2020-10-30 18:30:11', 0, 1),
(8, '2020103000008', 13, 500, 'JPY', 1, 'Purchase subscription memberId #13', 'https://api.omise.co/payments/paym_test_5lpk55pou8wvz4yoq1d/authorize', 'chrg_test_5lpk55pl02zy8gt4bn2', '2020-10-30 18:43:58', 0, 1),
(9, '2020112000009', 14, 500, 'JPY', 1, 'Purchase subscription memberId #14', '', '', '2020-11-20 10:18:28', 0, 1),
(10, '2020112000010', 15, 500, 'JPY', 1, 'Purchase subscription memberId #15', '', '', '2020-11-20 10:30:13', 0, 1),
(11, '2020112000011', 16, 500, 'JPY', 1, 'Purchase subscription memberId #16', '', '', '2020-11-20 10:34:28', 0, 1),
(12, '2020112000012', 17, 500, 'JPY', 1, 'Purchase subscription memberId #17', '', '', '2020-11-20 14:38:53', 0, 2),
(13, '2020112000013', 18, 500, 'JPY', 1, 'Purchase subscription memberId #18', '', '', '2020-11-20 14:52:24', 0, 2),
(14, '2020112000014', 19, 500, 'JPY', 1, 'Purchase subscription memberId #19', '', '', '2020-11-20 15:01:39', 0, 2),
(15, '2020112000015', 20, 500, 'JPY', 1, 'Purchase subscription memberId #20', '', '', '2020-11-20 15:07:27', 0, 2),
(16, '2020112300016', 21, 500, 'JPY', 1, 'Purchase subscription memberId #21', '', '', '2020-11-23 14:39:10', 0, 2),
(17, '2020112500017', 22, 500, 'JPY', 0, 'Purchase subscription memberId #22', '', '', '2020-11-25 10:59:37', 0, 2),
(18, '2020112500018', 22, 500, 'JPY', 1, 'Purchase subscription memberId #22', '', '', '2020-11-25 13:44:19', 0, 2),
(19, '2020120100019', 22, 500, 'JPY', 0, 'Purchase subscription memberId #22', '', '', '2020-12-01 15:05:57', 1, 0),
(20, '2020120100020', 22, 500, 'JPY', 1, 'Purchase subscription memberId #22', '', '', '2020-12-01 15:09:52', 1, 2),
(21, '2020120200021', 23, 500, 'JPY', 1, 'insufficient funds in the account or the card has reached the credit limit', '', '', '2020-12-02 10:08:55', 0, 2),
(22, '2021053100022', 24, 500, 'JPY', 1, 'Purchase subscription memberId #24', '', '', '2021-05-31 14:38:54', 0, 2),
(23, '2021053100023', 25, 500, 'JPY', 1, 'Purchase subscription memberId #25', '', '', '2021-05-31 14:48:49', 0, 2),
(24, '2021053100024', 26, 500, 'JPY', 1, 'Purchase subscription memberId #26', '', '', '2021-05-31 15:35:21', 0, 2),
(25, '2021053100025', 27, 500, 'JPY', 1, 'Purchase subscription memberId #27', '', '', '2021-05-31 15:45:25', 0, 2),
(26, '2021053100026', 28, 500, 'JPY', 1, 'Purchase subscription memberId #28', '', '', '2021-05-31 16:11:23', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `token` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `isUse` tinyint(3) NOT NULL DEFAULT '0',
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reset_password`
--

INSERT INTO `reset_password` (`id`, `member_id`, `token`, `isUse`, `createdtime`) VALUES
(1, 10, '8758a378872add2df7cd4ac3ef17475a', 0, '2020-10-28 15:18:12'),
(2, 10, 'f56407c8737755a4161b024857b39c88', 0, '2020-10-28 15:21:37'),
(3, 10, 'eb8218b71cda24069e172f395dfdefe5', 0, '2020-10-28 15:24:34'),
(4, 10, 'be92a3cf6792d0cac5d8af53d35e9722', 1, '2020-10-28 15:26:25'),
(5, 10, '4043ba28069bf591c254fb22a7037c03', 0, '2020-10-30 15:35:15'),
(6, 10, '4ff27e27b0c78403d5c41127215829e2', 0, '2020-10-30 15:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `sociallink`
--

CREATE TABLE `sociallink` (
  `id` int(11) NOT NULL,
  `ig` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tw` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `yk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sociallink`
--

INSERT INTO `sociallink` (`id`, `ig`, `tw`, `fb`, `line`, `yt`, `yk`) VALUES
(1, 'https://wwww.instagram.com', 'https://twitter.com', 'https://facebook.com', 'https://line.me/en/', 'https://youtube.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE `songs` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `stream_preview` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stream_full` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `duration` double NOT NULL,
  `isActive` tinyint(3) NOT NULL DEFAULT '1',
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_item` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `album_id`, `stream_preview`, `stream_full`, `link`, `duration`, `isActive`, `datasearch`, `createdtime`, `order_item`) VALUES
(1, 1, 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 'testx', 6, 1, '', '2020-09-14 17:13:12', 0),
(2, 1, 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-16.mp', 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 300, 1, 'song2artist1100$', '2020-09-24 17:45:16', 1),
(3, 2, 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3', 360, 1, 'song3artist3200', '2020-09-24 17:45:51', 0);

-- --------------------------------------------------------

--
-- Table structure for table `songs_txt`
--

CREATE TABLE `songs_txt` (
  `id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artist` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `songs_txt`
--

INSERT INTO `songs_txt` (`id`, `song_id`, `name`, `artist`, `price`, `lg`) VALUES
(1, 1, 'testx', 'testx', 'testx', 'en'),
(2, 2, 'song2', 'artist 1', '100$', 'en'),
(3, 3, 'song3', 'artist 3', '200', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `first_name`, `last_name`) VALUES
(1, 'alekpr', 'alek.pr@gmail.com', 'direk', 'prama');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `coverUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stream_preview` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stream_full` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `isActive` tinyint(3) NOT NULL DEFAULT '1',
  `datasearch` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdtime` datetime NOT NULL,
  `updatedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_item` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `coverUrl`, `thumUrl`, `stream_preview`, `stream_full`, `link`, `year`, `isActive`, `datasearch`, `createdtime`, `order_item`) VALUES
(1, '/get-file-upload?f=70d9c8fa6aa60257.png', '/get-file-upload?f=50b328ebf34af54f.png', '347119375', '347119375', 'link purchasex', 2020, 1, 'namexpricex', '2020-09-15 11:33:43', 1),
(2, '/get-file-upload?f=d4b7eabc45c1a20b.png', '/get-file-upload?f=6a9c9fd005f5a141.png', 'test', 'test', 'test', 2020, 1, 'video1100thb+tax', '2020-11-24 14:08:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos_txt`
--

CREATE TABLE `videos_txt` (
  `id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lg` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos_txt`
--

INSERT INTO `videos_txt` (`id`, `video_id`, `name`, `price`, `lg`) VALUES
(1, 1, 'namex', 'pricex', 'en'),
(2, 2, 'video 1', '100 THB + tax', 'en');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `albums_txt`
--
ALTER TABLE `albums_txt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ebirthday_cards`
--
ALTER TABLE `ebirthday_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ebirthday_sends`
--
ALTER TABLE `ebirthday_sends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goods_txt`
--
ALTER TABLE `goods_txt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `good_id` (`good_id`),
  ADD KEY `lg` (`lg`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marketing_scripts`
--
ALTER TABLE `marketing_scripts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `member_credential`
--
ALTER TABLE `member_credential`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `memberId` (`memberId`);

--
-- Indexes for table `member_social`
--
ALTER TABLE `member_social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metetags`
--
ALTER TABLE `metetags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_txt`
--
ALTER TABLE `news_txt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id` (`news_id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_txt`
--
ALTER TABLE `photo_txt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photo_id` (`photo_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `sociallink`
--
ALTER TABLE `sociallink`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `songs_txt`
--
ALTER TABLE `songs_txt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos_txt`
--
ALTER TABLE `videos_txt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `albums_txt`
--
ALTER TABLE `albums_txt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ebirthday_cards`
--
ALTER TABLE `ebirthday_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ebirthday_sends`
--
ALTER TABLE `ebirthday_sends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `goods_txt`
--
ALTER TABLE `goods_txt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `marketing_scripts`
--
ALTER TABLE `marketing_scripts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `member_credential`
--
ALTER TABLE `member_credential`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `member_social`
--
ALTER TABLE `member_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `metetags`
--
ALTER TABLE `metetags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `news_txt`
--
ALTER TABLE `news_txt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `photo_txt`
--
ALTER TABLE `photo_txt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sociallink`
--
ALTER TABLE `sociallink`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `songs_txt`
--
ALTER TABLE `songs_txt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `videos_txt`
--
ALTER TABLE `videos_txt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
