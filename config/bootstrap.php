<?php

use DI\ContainerBuilder;
use Slim\App;

require_once __DIR__ . '/../vendor/autoload.php';
define('OMISE_PUBLIC_KEY', 'pkey_test_5ak536wym8ha4o9fc3a');
define('OMISE_SECRET_KEY', 'skey_test_5ak536wz69i4wwhfgfj');
define('OMISE_API_VERSION', '2019-05-29');

define('ONETIME_PURCHASE_AMT',500);
define('MONTHLY_PURCHASE_AMT',500);
define('CURRENCY_PURCHASE',"JPY");
define('RETURN_URI',"https://realme.qernels.com/charge/");
define('FORGOT_URI',"https://realme.qernels.com/forgot-password/");
define("TEST_MODE",true);
$containerBuilder = new ContainerBuilder();

// Set up settings
$containerBuilder->addDefinitions(__DIR__ . '/container.php');

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// Create App instance
$app = $container->get(App::class);

// Register routes
(require __DIR__ . '/routes.php')($app);

// Register middleware
(require __DIR__ . '/middleware.php')($app);

return $app;