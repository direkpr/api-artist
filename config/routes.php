<?php
use App\Action\PreflightAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    $app->get('/share/news/{id}', \App\Action\ShareNewsAction::class)->setName('share-news');

    $app->get("/update-member-recurring",\App\Action\Jobs\UpdateRecurringAction::class)->setName("UpdateRecurring");
    $app->get("/send-ebirthday-cards",\App\Action\Jobs\SendEBirthdayCardAction::class)->setName("Send-eBirthday-Cards");

    $app->get('/test', \App\Action\HomeAction::class)->setName('home');
    $app->get('/', \App\Action\HomeAction::class)->setName('home');
    $app->get("/get-file-upload",\App\Action\UploadReadAction::class)->setName('get-file-data');
    $app->options('/get-file-upload', PreflightAction::class);

    $app->get("/get-photo-thumb",\App\Action\ThumbnailReadAction::class)->setName('get-thumb-data');
    $app->options('/get-photo-thumb', PreflightAction::class);

    $app->get('/api/users/{id}', \App\Action\UserReadAction::class)->setName('users-get');
    $app->options('/api/users/{id}', PreflightAction::class);
    $app->post('/api/users', \App\Action\UserCreateAction::class)->setName('users-post');
    $app->options('/api/users', PreflightAction::class);

    $app->get('/api/members/recurring', \App\Action\MemberRecurringAllAction::class)->setName('members-get-recurring');
    $app->options('/api/members/recurring', PreflightAction::class);
    $app->get('/api/members/renew-list', \App\Action\MemberRenewAllAction::class)->setName('members-get-renew');
    $app->options('/api/members/renew-list', PreflightAction::class);
    $app->get('/api/members/ebirthday-list', \App\Action\EBirthdaySendAllAction::class)->setName('members-get-ebirthday');
    $app->options('/api/members/ebirthday-list', PreflightAction::class);

    $app->get('/api/members', \App\Action\MemberAllAction::class)->setName('members-get');
    $app->post('/api/members', \App\Action\MemberCreateAction::class)->setName('members-post');
    $app->options('/api/members', PreflightAction::class);

    $app->get('/api/admin/members', \App\Action\MemberAdminAllAction::class)->setName('admin-members-get');
    $app->post('/api/admin/members', \App\Action\MemberAdminCreateAction::class)->setName('admin-members-post');
    $app->options('/api/admin/members', PreflightAction::class);
    $app->get('/api/admin/members/{id}', \App\Action\MemberAdminReadAction::class)->setName('admin-members-get');
    $app->options('/api/admin/members/{id}', PreflightAction::class);
    $app->post('/api/admin/members/{id}', \App\Action\MemberAdminUpdateAction::class)->setName('admin-members-update');
    $app->delete('/api/admin/members/{id}', \App\Action\MemberAdminDeleteAction::class)->setName('admin-members-delete');


    $app->post('/api/members/renew', \App\Action\MemberRenewAction::class)->setName('members-renew-purchase');
    $app->options('/api/members/renew', PreflightAction::class);
    $app->get('/api/members/{id}', \App\Action\MemberReadAction::class)->setName('members-get');
    $app->options('/api/members/{id}', PreflightAction::class);
    $app->post('/api/members/{id}', \App\Action\MemberUpdateAction::class)->setName('members-update');

    $app->post('/api/login', \App\Action\MemberLoginAction::class)->setName('member-login');
    $app->options('/api/login', PreflightAction::class);

    $app->post('/api/login-withsocial', \App\Action\SocialLoginAction::class)->setName('member-login-withsocial');
    $app->options('/api/login-withsocial', PreflightAction::class);

    $app->post('/api/login-with-token', \App\Action\LoginWithTokenAction::class)->setName('member-login-with-token');
    $app->options('/api/login-with-token', PreflightAction::class);

    //login-with-token

    $app->post('/api/connect-withsocial', \App\Action\SocialConnectAction::class)->setName('member-connect-withsocial');
    $app->options('/api/connect-withsocial', PreflightAction::class);

    $app->post('/api/disconnect-withsocial', \App\Action\SocialDisconnectAction::class)->setName('member-disconnect-withsocial');
    $app->options('/api/disconnect-withsocial', PreflightAction::class);

    $app->post('/api/check-social-link', \App\Action\SocialLinkAction::class)->setName('member-link-withsocial');
    $app->options('/api/check-social-link', PreflightAction::class);
    //check-social-link

    $app->post('/api/upload-file', \App\Action\UploadFileAction::class)->setName('upload-file');
    $app->options('/api/upload-file', PreflightAction::class);

    $app->get('/api/langs', \App\Action\LangsReadAction::class)->setName('get-all-langs');
    $app->options('/api/langs', PreflightAction::class);

    $app->group('/api/contact',function(RouteCollectorProxy $group){
        $group->post("/send",\App\Action\ContactCreateAction::class)->setName("create-contact");
        $group->get("",\App\Action\ContactListAction::class)->setName("list-contact");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\ContactReadAction::class)->setName("get-contact");
        $group->delete("/{id}",\App\Action\ContactDeleteAction::class)->setName("delete-contact");
        $group->options('/{id}', PreflightAction::class);
    });


    $app->group('/api/news',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\NewsCreateAction::class)->setName("create-news");
        $group->get("",\App\Action\NewsListAction::class)->setName("list-news");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\NewsReadAction::class)->setName("get-news");
        $group->patch("/{id}",\App\Action\NewsUpdateAction::class)->setName("update-news");
        $group->delete("/{id}",\App\Action\NewsDeleteAction::class)->setName("delete-news");
        $group->options('/{id}', PreflightAction::class);
    });


    $app->group('/api/videos',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\VideoCreateAction::class)->setName("create-videos");
        $group->get("",\App\Action\VideoListAction::class)->setName("list-videos");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\VideoReadAction::class)->setName("get-videos");
        $group->patch("/{id}",\App\Action\VideoUpdateAction::class)->setName("update-videos");
        $group->delete("/{id}",\App\Action\VideoDeleteAction::class)->setName("delete-videos");
        $group->options('/{id}', PreflightAction::class);
        $group->patch("/{id}/change-order",\App\Action\VideoUpdateOrderAction::class)->setName("update-video-order");
        $group->options('/{id}/change-order', PreflightAction::class);
    });


    $app->group('/api/songs',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\SongCreateAction::class)->setName("create-songs");
        $group->get("",\App\Action\SongListAction::class)->setName("list-songs");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\SongReadAction::class)->setName("get-songs");
        $group->patch("/{id}",\App\Action\SongUpdateAction::class)->setName("update-songs");
        $group->delete("/{id}",\App\Action\SongDeleteAction::class)->setName("delete-songs");
        $group->options('/{id}', PreflightAction::class);
        $group->patch("/{id}/change-order",\App\Action\SongUpdateOrderAction::class)->setName("update-songs-order");
        $group->options('/{id}/change-order', PreflightAction::class);
    });

    $app->group('/api/albums',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\AlbumCreateAction::class)->setName("create-album");
        $group->get("",\App\Action\AlbumListAction::class)->setName("list-album");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\AlbumReadAction::class)->setName("get-album");
        $group->patch("/{id}",\App\Action\AlbumUpdateAction::class)->setName("update-album");
        $group->delete("/{id}",\App\Action\AlbumDeleteAction::class)->setName("delete-album");
        $group->options('/{id}', PreflightAction::class);
    });


    $app->group('/api/photo',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\PhotoCreateAction::class)->setName("create-photo");
        $group->get("",\App\Action\PhotoListAction::class)->setName("list-photo");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\PhotoReadAction::class)->setName("get-photo");
        $group->patch("/{id}",\App\Action\PhotoUpdateAction::class)->setName("update-photo");
        $group->delete("/{id}",\App\Action\PhotoDeleteAction::class)->setName("delete-photo");
        $group->options('/{id}', PreflightAction::class);
    });

    $app->group('/api/ebirthday-cards',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\EbirthdayCreateAction::class)->setName("create-ebirthday-cards");
        $group->get("",\App\Action\EbirthdayListAction::class)->setName("list-ebirthday-cards");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\EbirthdayReadAction::class)->setName("get-ebirthday-cards");
        $group->patch("/{id}",\App\Action\EbirthdayUpdateAction::class)->setName("update-ebirthday-cards");
        $group->delete("/{id}",\App\Action\EbirthdayDeleteAction::class)->setName("delete-ebirthday-cards");
        $group->options('/{id}', PreflightAction::class);
    });
    

    $app->group('/api/goods',function(RouteCollectorProxy $group){
        $group->post("",\App\Action\GoodsCreateAction::class)->setName("create-goods");
        $group->get("",\App\Action\GoodsListAction::class)->setName("list-goods");
        $group->options('', PreflightAction::class);
        $group->get("/{id}",\App\Action\GoodsReadAction::class)->setName("get-goods");
        $group->patch("/{id}",\App\Action\GoodsUpdateAction::class)->setName("update-goods");
        $group->delete("/{id}",\App\Action\GoodsDeleteAction::class)->setName("delete-goods");
        $group->options('/{id}', PreflightAction::class);
    });

    $app->group('/api/social-link',function(RouteCollectorProxy $group){
        $group->get("/{id}",\App\Action\SocialLinkReadAction::class)->setName("get-social-link");
        $group->patch("/{id}",\App\Action\SocialLinkUpdateAction::class)->setName("update-social-link");
        $group->options('/{id}', PreflightAction::class);
    });

    $app->group('/api/metetag',function(RouteCollectorProxy $group){
        $group->get("/{id}",\App\Action\MetetagReadAction::class)->setName("get-metetag");
        $group->patch("/{id}",\App\Action\MetetagUpdateAction::class)->setName("update-metetag");
        $group->options('/{id}', PreflightAction::class);
    });

    $app->group('/api/marketingscripts',function(RouteCollectorProxy $group){
        $group->get("/{id}",\App\Action\MarketingScriptReadAction::class)->setName("get-marketingscripts");
        $group->patch("/{id}",\App\Action\MarketingScriptUpdateAction::class)->setName("update-marketingscripts");
        $group->options('/{id}', PreflightAction::class);
    });



    //site
    $app->group("/api-site",function(RouteCollectorProxy $group){
        $group->get("/news",\App\Action\Site\NewsListAction::class)->setName("get-site-news");
        $group->options('/news', PreflightAction::class);

        $group->get("/photos",\App\Action\Site\PhotoListAction::class)->setName("get-site-photos");
        $group->options('/photos', PreflightAction::class);

        $group->get("/goods",\App\Action\Site\GoodsListAction::class)->setName("get-site-goods");
        $group->options('/goods', PreflightAction::class);

        $group->get("/musics",\App\Action\Site\MusicListAction::class)->setName("get-site-music");
        $group->options('/musics', PreflightAction::class);

        $group->get("/videos",\App\Action\Site\VideoListAction::class)->setName("get-site-video");
        $group->options('/videos', PreflightAction::class);

        $group->get("/social-link/{id}",\App\Action\SocialLinkReadAction::class)->setName("get-social-link");
        $group->options('/social-link/{id}', PreflightAction::class);

        $group->get("/marketingscripts/{id}",\App\Action\MarketingScriptReadAction::class)->setName("get-marketingscripts");
        $group->options('/marketingscripts/{id}', PreflightAction::class);

        $group->post("/forgot-password",\App\Action\Site\ForgotAction::class)->setName("forgot-password");
        $group->options('/forgot-password', PreflightAction::class);

        $group->post("/change-password",\App\Action\Site\ChangePasswordAction::class)->setName("change-password");
        $group->options('/change-password', PreflightAction::class);

        $group->get("/news/{id}",\App\Action\NewsReadAction::class)->setName("get-news-byId");
        $group->options('/news/{id}', PreflightAction::class);

    });

    //payment
    $app->group("/omise",function(RouteCollectorProxy $group){
        $group->get("/test",\App\Action\Payment\TestAction::class)->setName("test-payment");
        $group->options('/test', PreflightAction::class);

        $group->post("/charge",\App\Action\Payment\ChargeAction::class)->setName("charge-payment");
        $group->options('/charge', PreflightAction::class);

        $group->post("/cancel-subscriber",\App\Action\Payment\UnsubscribeAction::class)->setName("cancel-subscribe");
        $group->options('/cancel-subscriber', PreflightAction::class);

        $group->post("/schedule-charge",\App\Action\Payment\ScheduleChargeAction::class)->setName("schedule-charge-payment");
        $group->options('/schedule-charge', PreflightAction::class);


        $group->get("/invoice/{invoice}",\App\Action\Payment\InvoiceReadAction::class)->setName("get-invoice-payment");
        $group->options('/invoice/{invoice}', PreflightAction::class);

        $group->get("/invoice-update/{invoice}",\App\Action\Payment\InvoiceUpdateAction::class)->setName("update-invoice-payment");
        $group->options('/invoice-update/{invoice}', PreflightAction::class);
    });
    
};