<?php
use Selective\BasePath\BasePathMiddleware;
use Slim\App;
use Slim\Middleware\ErrorMiddleware;

return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    $app->add(\App\Middleware\CorsMiddleware::class); // <--- here

    $container = $app->getContainer();
    $app->add(new Tuupola\Middleware\JwtAuthentication([
        "path" => ["/api", "/admin"],
        "ignore" => ["/api/contact/send","/api/login","/api/langs","/api/members", "/admin/ping","/api/login-withsocial","/api/login-with-token"],
        "secret" => $container->get('settings')['secret']
    ]));

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

   // $app->add(BasePathMiddleware::class); // <--- here

    // Catch exceptions and errors
    $app->add(ErrorMiddleware::class);
};