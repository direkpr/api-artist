<?php
try {
    (require __DIR__ . '/../config/bootstrap.php')->run();
} catch (Exception $e) {    
  //We display a error message
  die( json_encode(array("status" => "failed", "message" => "This action is not allowed"))); 
}